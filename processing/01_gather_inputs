#!/bin/bash

# 01_gather_inputs : scan source data, and assemble file locations (with paired
# water references) into matlab-style cell array declarations. Also generates
# subsets for debugging individual sites, or one representative dataset for
# each site.

# Accepts one parameter, "mode" : PRESS, GABA_68, GABA_80 or "all".
#
# If nothing is specified, default to GABA_68
# 
# If "mode" is "all", three files (generated_*m) will be created. For all other
# modes, output should be redirected to a file, eg:
#
# ./01_gather_inputs > generated_gaba68.m

mode='GABA_68'


case "${1}" in
  PRESS|GABA_*)
    # first param might be a mode, in which case we interpret it as such then proceed as normal
    mode="$1";
    shift;
    ;;

  all)
    # first param might also be "all", in which case generate script for all three mdoes
    ${0} PRESS   > generated_press.m
    ${0} GABA_68 > generated_gaba68.m
    ${0} GABA_80 > generated_gaba80.m
    exit
    ;;
  # if first parameter isn't one of the above, just take any remaining parameters to be a list of files to deal with (this script calls itself recursively)
esac

src="/unknown"

# src should be adjusted to wherever the original data are stored. this is site-specific, and should be altered accordlingly {{{

case $(hostname) in 

  *proc1)  # processing workstation (remote access; BBB 4th floor)
    case ${mode} in # {{{
      PRESS*) # big PRESS
        src=/data/current/other/big_gaba/big_press
        ;;
      GABA*)     # big GABA
        src=/data/current/other/big_gaba
        ;;
    esac            # }}}
    ;;

  *spectre) # spectroscopy workstation (Alex's office)
    case ${mode} in # {{{
      PRESS*) # big PRESS
        src=/data/current/other/big_gaba/big_press
        ;;
      GABA*)     # big GABA
        src=/data/big_gaba
        ;;
    esac            # }}}
    ;;

esac

# }}}

if [ ! -d "${src}" ] ; then 
  echo "Source folder \"${src}\" does not exist (for $(hostname) in ${mode} mode)." >&2
  exit -1
fi

if (( $# == 0 )) ; then
  echo "subs={";
  name_pat="this_should_not_match_anything";
  case ${mode} in
    GABA_68|PRESS|GABA_80)
      name_pat=${mode}
      ;;
  esac
  # Gannet expects SDAT for philips, not spar
  find ${src}/[PGS]* -maxdepth 2 -type f -and \( -iname \*\.dat -or -iname \*\.7 -or -iname \*\.sdat \) -and -name \*${name_pat}\* -and -not \( -iname \*_ref\.\* -or -iname \*_H2O\.\* \) -and -not \( -wholename \*REJECT\* \) | sort -n | xargs $0 | sed '$ s/.$//' # sed: remove last char from last line
  echo "}";

  if [[ ! -f /dev/stdout ]] ; then
    echo -e "\n\nYou probably want to redirect this output to a file, for example:\n$0 > generated_gaba68.m" >&2
  fi
else
  n=0;
  last_dir="nothing";
  declare -a one_per_site
  declare -a sites

  while (( $# > 0 )) ; do

    (( n = $n + 1 ))
    met="$1"
    ref=""

    case ${met} in
      *_act*)
        ref=${met/_act/_ref};
        ;;
      *\.dat)
        ref=${met/.dat/_H2O.dat}
        ;;
      *\.7)
        ref=""
        ;;
      *)
        echo "Water reference: pattern match faiiled for ${met}"
        ;;
    esac

    d=$(dirname "${met}");
    dd=$(dirname "${d}");

    suffix=" % ${n} ";

    persite_target="";
    persite_first=0;

    if [[ "${dd}" != "${last_dir}" ]] ; then
      echo "${dd}" >&2
      bd=$(basename ${dd})
      suffix="${suffix} <== site ${bd}"
      persite_first=1;
      #one_per_site+=("${met}")
      last_dir="${dd}"
    fi

    if [[ -z "${ref}" ]] && [ -f "${met}" ]; then
      line="  '${met}',${suffix}";
      echo "${line}";
    elif [ -f "$ref" ] && [ -f "${met}" ] ; then
      line="  {'${met}','${ref}'},${suffix}"
      echo "${line}";
    else
      echo "% Some parts missing? ${met} ${ref}${suffix}";
    fi

    if (( ${persite_first} > 0 )) ; then
      one_per_site+=("${line}")
      sa="site_${bd}"
      sites+=(${sa})
      declare -a ${sa}
      persite_first=0;
    fi

    eval "$sa+=(\"${line}\")"

    shift;

  done
  echo '};'
  echo 'one_per_site={ % one per site, for debugging purposes'
  for s in "${one_per_site[@]}" ; do
    echo "${s}";
  done

  for ss in "${sites[@]}" ; do
    echo "}";
    echo "${ss}={ % one per site, for debugging purposes";
    # declare -a x=( "${!ss}" );
    x="${ss}[@]"
    for s in "${!x}" ; do
      echo "${s}"
    done
  done
fi
