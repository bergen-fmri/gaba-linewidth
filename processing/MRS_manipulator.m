classdef MRS_manipulator < MRS_interconnect_FIDA
    % MRS_manipulator : manipulate MRS data in various ways
    % Alexander R Craven, University of Bergen, 2023
    %
    % Key functions of interest are:
    %
    % linebroadening - applies mixed Lorentz/Gauss linebroadening to nominated
    %                  (sub)spectra
    %          getLW - estimates linewidth from a reference peak
    %         getSNR - estimates SNR from a reference peak
    % 
    % Most of these functions wrap basic FIDA operations

    properties (Access = protected)
        clean = [];
        noise_model = [];
        noise_model_basic = [];
    end

    methods % {{{

        function self = MRS_manipulator(d, varargin)
            % MRS_interconnect_base constructor : store parameters
            %
            % Stores independent copies of clean (unfiltered) and processed spectra

            self = self@MRS_interconnect_FIDA(d, varargin{:}); % fall back on default constructor
            self.clean = MRS_manipulator.deepish_copy(self.processed_subspectra);
            self.processed_subspectra = MRS_manipulator.deepish_copy(self.processed_subspectra);
        end

        function reset(self)
            % reset : revert to stored copy of clean (unfiltered) spectra

            self.processed_subspectra = MRS_manipulator.deepish_copy(self.clean);
            self.push_processed_to_MRS_struct();
        end

        function push_processed_to_MRS_struct(self)
            % push_processed_to_MRS_struct puts processed (filtered) spectra
            % back into the stored Gannet-esque MRS_struct
            %
            % MRS_interconnect_* originally used a Gannet-like structure as the
            % primary data storage, although that has certain limitations
            % (particularly for non-edited spectra).
            %
            % The current version uses a hybrid of Gannet-like structures and
            % FID-A structures; this function supports exchange of information
            % between the two.

            dbg = 0;

            if self.multi_echo
                diffSpec = self.processed_subspectra('diff');
                sumSpec = self.processed_subspectra('sum');

                subSpec1_svd = self.processed_subspectra('on');
                subSpec2_svd = self.processed_subspectra('off');
                if isKey(self.processed_subspectra,'on_noSVD')
                  subSpec1_nosvd = self.processed_subspectra('on_noSVD');
                  subSpec2_nosvd = self.processed_subspectra('off_noSVD');
                end

                subSpec1 = subSpec1_svd;
                subSpec2 = subSpec2_svd;

            else
                if isKey(self.processed_subspectra,'off_noSVD')
                  subSpec1_nosvd = self.processed_subspectra('off_noSVD');
                end
                subSpec1_svd = self.processed_subspectra('off');
                subSpec2 = [];
                subSpec2_svd = [];

                subSpec1 = subSpec1_svd;
                sumSpec = subSpec1_svd;
                diffSpec = subSpec1_svd;
            end

            if self.water
                outw = self.processed_subspectra('ref');
            else
                outw = 0;
            end

            % from MRS_interconnect_FIDA::preprocess

            v1 = self.MRS_struct.p.Vox{1};

            if iscell(self.MRS_struct.p.target)
                t1 = self.MRS_struct.p.target{1};
            else
                t1 = self.MRS_struct.p.target;
            end

            % FID-A uses a fftshift(ifft(...)) pattern, which results in conjugate-flipped spectra when compared with Gannet/LCModel preprocessing
            % hence, need to take conj of subSpec1, subSpec2, diffSpec
            self.MRS_struct.spec.(v1).(t1).on = fftshift(fft(conj(subSpec1.fids), [], subSpec1.dims.t), subSpec1.dims.t);

            if ~isempty(subSpec2)
                self.MRS_struct.spec.(v1).(t1).off = fftshift(fft(conj(subSpec2.fids), [], subSpec2.dims.t), subSpec2.dims.t);
            else
                % Wrangle unedited (PRESS) data into the structure: use edit-off component, then set edited component to all 0s
                self.MRS_struct.spec.(v1).(t1).off = self.MRS_struct.spec.(v1).(t1).on;
                self.MRS_struct.spec.(v1).(t1).on = 0 * self.MRS_struct.spec.(v1).(t1).on;
            end

            if dbg > 2 % {{{
                % debug figure to ensure we have the right conj sense
                figure();
                subplot(2, 2, 1); plot(real(fftshift(fft(diffSpec.fids)))); title('saving this version');
                subplot(2, 2, 2); plot(real(fftshift(fft(conj(diffSpec.fids)))));
                subplot(2, 2, 3); plot(real(diffSpec.specs))
                subplot(2, 2, 4); plot(real(conj(diffSpec.specs)))
            end % }}}

            self.MRS_struct.spec.(v1).(t1).diff = fftshift(fft(conj(diffSpec.fids), [], diffSpec.dims.t), diffSpec.dims.t);

            if self.water
                self.MRS_struct.spec.(v1).water = fftshift(fft(conj(outw.fids), [], outw.dims.t), outw.dims.t);
            end

            % Gannet requires that these elements are row-vectors; it doesn't always work out this way, depending on ordering of dimensions above. so, check and correct if necessary:
            if size(self.MRS_struct.spec.(v1).(t1).diff, 1) > size(self.MRS_struct.spec.(v1).(t1).diff, 2)
                self.MRS_struct.spec.(v1).(t1).on = self.MRS_struct.spec.(v1).(t1).on';
                self.MRS_struct.spec.(v1).(t1).off = self.MRS_struct.spec.(v1).(t1).off';
                self.MRS_struct.spec.(v1).(t1).diff = self.MRS_struct.spec.(v1).(t1).diff';

                if self.water
                    self.MRS_struct.spec.(v1).water = self.MRS_struct.spec.(v1).water';
                end
            end

        end

        function linebroadening(self, subspec_filter, lb, lg)
            % wrapper for op_filter_lg, which extends the FID-A op_filter
            %
            % op_filter_lg.m
            % Line-broadening with adjustable lorentzian/gaussian ratio
            % Alexander R Craven, University of Bergen, 2023
            % 
            % Based on the original (pure Lorentzian) op_filter.m
            % Jamie Near, McGill University 2014.
            % 
            % USAGE:
            % [out,lor]=op_filter(in,lb,lg);
            % 
            % DESCRIPTION:
            % Perform line broadening by multiplying the time domain signal by
            % a nominated mixture of decay functions (Lorentzian and Gaussian)
            % 
            % INPUTS:
            % in     = input data in matlab structure format.
            % lb     = line broadening factor in Hz.
            % lg     = lorentz-gauss lineshape param (0.0-1.0)
            %          (0: pure lorentz, 1: pure gauss, or mix; default 0)
            %
            % OUTPUTS:
            % out    = Output following alignment of averages.  
            % lorgau = Time domain filter envelope that was applied.


            if (nargin < 2)
                subspec_filter = [];
            end

            if (nargin < 3)
                error('must specify lb (Hz)');
            end

            if (nargin < 4)
                lg=0; % default to pure Lorentz
            end

            ss = keys(self.processed_subspectra);

            processed = MRS_manipulator.deepish_copy(self.processed_subspectra);

            for si = 1:length(ss)
                ssn = ss{si};

                if ~isempty(subspec_filter) &&~any(strcmp(ssn, subspec_filter))
                    continue;
                end

                f = processed(ssn);
                [ret, ~] = op_filter_lg(f, lb, lg);
                processed(ssn) = ret;
            end

            self.processed_subspectra = processed;

            self.push_processed_to_MRS_struct();
        end

        function ret = getLW(self, subspec_filter, ppmrange)
            % wrapper for FID-A op_getLW
            %
            % USAGE:
            % subspec_filter : optionally, limit to nominated subspecs
            % ppmrange: optionally, search for peak in the specified range
            %
            % ppmrange defaults to 4.4-5.0 (water) for ref spectra, 1.9-2.1
            % (NAA) for diff, 2.95-3.15 (Cre) otherwise
            %
            %
            % Wrapped op_getLW function: Jamie Near, McGill University 2014.
            %
            % USAGE:
            % [FWHM]=op_getLW(in,Refppmmin,Refppmmax,zpfactor);
            %
            % DESCRIPTION:
            % Estimates the linewidth of a reference peak in the spectrum.  By
            % default, the reference peak is water, between 4.4 and 5.0 ppm.
            % Two methods are used to estimate the linewidth:  1.  FWHM is
            % measured by simply taking the full width at half max of the
            % reference peak.  2.  The FWHM is measured by fitting the
            % reference peak to a lorentzian lineshape and determine the FWHM
            % of the best fit.  The output FWHM is given by the average of
            % these two measures.
            %
            % INPUTS:
            % in         = input spectrum in structure format.
            % Refppmmin  = Min of frequency range (ppm) in which to search for reference peak.
            %                  (Optional.  Default = 4.4 ppm);
            % Refppmmax  = Max of frequency range to (ppm) in which search for reference peak
            %                  (Optional.  Default = 5/3 ppm per Tesla B0);
            % zpfactor   = zero-padding factor (used for method 1.)
            %                  (Optional.  Default = 8);
            %
            % OUTPUTS:
            % FWHM       = Estimated linewidth of the input spectrum (in Hz).

            if (nargin < 2)
                subspec_filter = [];
            end

            if (nargin < 3)
                ppmrange = [];
            end

            ss = keys(self.processed_subspectra);

            x = struct();

            for si = 1:length(ss)
                ssn = ss{si};

                if ~isempty(subspec_filter) &&~any(strcmp(ssn, subspec_filter))
                    continue;
                end

                f = self.processed_subspectra(ssn);

                if isempty(ppmrange)

                    if strcmp(ssn, 'ref')
                        lw = op_getLW(f, 4.4, 5.0);
                    elseif ~strcmp(ssn, 'diff')
                        lw = op_getLW(f, 2.95, 3.15) % Cre
                    else
                        lw = op_getLW(f, 1.9, 2.1) % NAA
                    end

                else
                    lw = op_getLW(f, min(ppmrange), max(ppmrange))
                end

                x.(ssn) = lw
                % row=struct('subspec',ssn,'SNR',SNR,'signal',signal,'noisesd','noisesd');
                %all_results.append(row);
            end

            if isempty(subspec_filter)
                ret = x;
            else
                ret = lw;
            end

        end

        function ret = getSNR(self, subspec_filter)
            % wrapper for FID-A op_getSNR
            %
            % USAGE:
            % subspec_filter : optionally, limit to nominated subspecs
            %
            % getSNR uses FID-A default (1.8-2.2 ppm : NAA) peak, except for
            % ref spectra where it looks for a peak in the water range (4.4-4.9
            % ppm)
            %
            %
            % Wrapped op_getSNR function: Jamie Near, McGill University 2014.
            %
            % USAGE:
            % [SNR]=op_getSNR(in,NAAppmmin,NAAppmmax,noiseppmmin,noiseppmmax);
            %
            % DESCRIPTION:
            % Find the SNR of the NAA peak in a spectrum.
            %
            % INPUTS:
            % in             = input data in matlab structure format
            % NAAppmmin      = min of frequncy range in which to search for NAA peak.
            %                  (Optional.  Default = 1.8 ppm);
            % NAAppmmax      = max of frequncy range in which to search for NAA peak.
            %                  (Optional.  Default = 2.2 ppm);
            % noiseppmmin    = min of frequency range in which to measure noise.
            %                  (Optional.  Default = -2 ppm);
            % noiseppmmax    = max of frequency range in which to measure noise.
            %                  (Optional.  Default = 0 ppm);
            %
            % OUTPUTS:
            % SNR            = Estimated SNR of the input spectrum.
            % signal         = The measured raw signal intensity
            % noisesd        = The measured noise standard deviation

            if (nargin < 2)
                subspec_filter = [];
            end

            ss = keys(self.processed_subspectra);

            all_results = datatable_ARC();

            row = [];

            for si = 1:length(ss)
                ssn = ss{si};

                if ~isempty(subspec_filter) &&~any(strcmp(ssn, subspec_filter))
                    continue;
                end

                f = self.processed_subspectra(ssn);

                if strcmp(ssn, 'ref')
                    [SNR, signal, noisesd] = op_getSNR(f, 4.4, 4.9); % use water not NAA
                else
                    [SNR, signal, noisesd] = op_getSNR(f);
                end

                row = struct('subspec', ssn, 'SNR', SNR, 'signal', signal, 'noisesd', noisesd);
                all_results.append(row);
            end

            if isempty(subspec_filter)
                ret = all_results;
            else
                ret = row;
            end

        end

        function noise_model_reset(self)
            % noise_model_learn : EXPERIMENTAL flush noise model samples
            %
            % This function is a experimental, and is not reported in any current works.

            self.noise_model = [];
        end

        function noise_model_show(self)
            % noise_model_learn : EXPERIMENTAL display current noise model samples
            %
            % This function is a experimental, and is not reported in any current works.

            figure(99); clf;
            mk = keys(self.noise_model);

            for ki = 1:length(mk)
                k = mk{ki};
                model = self.noise_model(k);

                [xx, ix] = sort(sqrt([model.samples.snr_delta]));
                yy = [model.samples.added];
                yy = yy(ix);

                plot(xx, yy, 'x', 'DisplayName', sprintf('Bin %d', k));

                if ~isempty(model.func)
                    zz = model.func(xx);
                    plot(xx, zz, ':', 'DisplayName', sprintf('Bin %d fit', k));
                end

                hold on;
            end

            legend();
        end

        function noise_model_learn(self, d_in, d_out, added)
            % noise_model_learn : EXPERIMENTAL update noise model with additional sample points
            %
            % This function is a experimental, and is not reported in any current works.

            bin = round(d_in.SNR / 50);

            if isempty(self.noise_model)
                self.noise_model = containers.Map('KeyType', 'double', 'ValueType', 'any');
            end

            if isKey(self.noise_model, bin)
                model = self.noise_model(bin);
            else
                model = struct('func', [], 'samples', []);
            end

            lim_samples = 50;
            sample = struct(...
                'snr_in', d_in.SNR, ...
                'snr_out', d_out.SNR, ...
                'snr_delta', d_in.SNR - d_out.SNR, ...
                'added', added ...
                );

            if isempty(model.samples)
                model.samples = sample;
            else

                if length(model.samples) > lim_samples
                    model.samples = model.samples(randperm(length(model.samples), lim_samples));
                end

                model.samples(end + 1) = sample;

                if length(model.samples) > 5
                    %ft=fittype('a + b*log(x)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b'});
                    %ft=fittype('a + b*x', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b'});
                    %ft=fittype('a + b*log(x-c)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b','c'});
                    %ft=fittype('a + b*exp(c./x)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b','c'});

                    xx = sqrt([model.samples.snr_delta]);
                    yy = [model.samples.added]; % log(1./v_added_noise);

                    valid = (xx > 0);
                    xx = xx(valid);
                    yy = yy(valid);

                    if length(model.samples) > 10
                        ft = fittype('a+b*log(x)+c*x', 'dependent', {'y'}, 'independent', {'x'}, 'coefficients', {'a', 'b', 'c'});
                        model.func = fit(xx', yy', ft, 'StartPoint', [0, 1, 1]);
                    else
                        % few points, simple linear fit
                        ft = fittype('a*x', 'dependent', {'y'}, 'independent', {'x'}, 'coefficients', {'a'});
                        model.func = fit(xx', yy', ft, 'StartPoint', [1]);
                    end

                end

            end

            self.noise_model(bin) = model;

        end

        function noise_to_add = noise_model_est(self, d_in, SNR_out)
            % noise_model_est : EXPERIMENTAL function determining modelled noise level required to achieve requested SNR
            %
            % This function is a experimental, and is not reported in any current works.

            noise_to_add = 50.; % initial guess

            if ~isempty(self.noise_model)
                bin = round(d_in.SNR / 50);

                if isKey(self.noise_model, bin)
                    model = self.noise_model(bin)

                    if ~isempty(model.func)
                        noise_to_add = model.func(sqrt(d_in.SNR - SNR_out));
                    end

                end

            end

        end

        function model_lw_noise(self, subspec_filter)
            % model_lw_noise : EXPERIMENTAL function relating nominal (applied) noise to achieved SNR
            %
            % This function is a experimental, and is not reported in any current works.

            if (nargin < 2)
                subspec_filter = 'diff';
            end

            fancy_noise_model = 0;

            dt = datatable_ARC();

            self.reset();
            SNR0 = self.getSNR(subspec_filter);

            noise_scale = 50.;
            cf = []
            total_err = [];
            cfs = {};
            models = [];
            its = 3;

            for it = 0:(its + 1)

                v_snr_pre = [];
                v_snr_post = [];
                v_snr_gain = [];
                v_added_noise = [];
                v_err = [];

                step = 0.2;
                lim = 8;

                %ft=fittype('a + b*log(x)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b'});
                ft = fittype('a+b*log(x)+c*x', 'dependent', {'y'}, 'independent', {'x'}, 'coefficients', {'a', 'b', 'c'});
                %ft=fittype('a + b*x', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b'});
                %ft=fittype('a + b*log(x-c)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b','c'});
                %ft=fittype('a + b*exp(c./x)', 'dependent',{'y'},'independent',{'x'}, 'coefficients',{'a','b','c'});

                for lb = 0.1:step:lim
                    %close all;
                    self.reset();
                    self.linebroadening(subspec_filter, lb);

                    dets = self.getSNR(subspec_filter);
                    dets.lb = lb;

                    for targ_ofs = [.9 1 1.1]

                        SNR_gain = dets.SNR - SNR0.SNR;
                        noise_defecit = (dets.signal / SNR0.SNR) - dets.noisesd;

                        if fancy_noise_model
                            noise_adjusted = self.noise_model_est(dets, SNR_gain);
                        elseif isempty(cf)
                            noise_adjusted = noise_scale * noise_defecit;
                        else
                            noise_adjusted = cf(sqrt(SNR_gain));
                        end

                        noise_adjusted = noise_adjusted * targ_ofs;

                        self.addNoise(subspec_filter, noise_adjusted);
                        d2 = self.getSNR(subspec_filter);

                        if fancy_noise_model
                            self.noise_model_learn(dets, d2, noise_adjusted);

                            % add the noise again, to help with our modelling
                            self.addNoise(subspec_filter, noise_adjusted);
                            d3 = self.getSNR(subspec_filter);
                            self.noise_model_learn(d2, d3, noise_adjusted);
                        end

                        dets.SNR_gain = SNR_gain;
                        dets.noise_defecit = noise_defecit;
                        dets.SNR_renoised = d2.SNR;
                        dets.SNR_renoise_gain = dets.SNR - d2.SNR;
                        dets.err = SNR0.SNR - d2.SNR;

                        v_snr_pre(end + 1) = dets.SNR;
                        v_snr_post(end + 1) = d2.SNR;
                        v_snr_gain(end + 1) = dets.SNR_renoise_gain;
                        v_added_noise(end + 1) = noise_adjusted;
                        v_err(end + 1) = dets.err;

                        dt.append(dets);
                    end

                end

                if fancy_noise_model
                    self.noise_model_show();
                end

                figure(98);
                clf;
                xx = sqrt(v_snr_gain);
                yy = v_added_noise; % log(1./v_added_noise);

                valid = (xx > 0);
                xx = xx(valid);
                yy = yy(valid);

                %yy=1./v_snr_gain;
                %yy=v_snr_gain.^(1/4)
                plot(xx, yy, 'b-o');
                P = polyfit(xx, yy, 2);
                zz = polyval(P, xx);

                %ft=fittype('exp1')
                cf = fit(xx', yy', ft);
                cfs{end + 1} = cf;
                zzz = cf(xx');
                cf.a;
                cf.b
                cf(1)

                hold on; plot(xx, zz, 'r:x');
                hold on; plot(xx, zzz, 'g:x');

                total_err(end + 1) = sum(abs(v_err));

                if (it == its)
                    % final iteration, redo with best
                    [~, best] = min(total_err);

                    if (best > 1)
                        cf = cfs(best - 1);
                    end

                    cf = cfs{best};
                    self.noise_model_basic = cf;
                end

            end

            dt.show();
            total_err
            SNR0.SNR
        end

        function addNoiseForSNRDelta(self, subspec_filter, SNR_gain)

            if isempty(self.noise_model_basic)
                error('must do model_lw_noise first');
            end

            self.addNoise(subspec_filter, self.noise_model_basic(sqrt(abs(SNR_gain))));
        end

        function addNoise(self, subspec_filter, sdnoise)
            % wrapper for op_addNoise
            %
            % Adds noise, drawn from the standard normal distribution (matlab: randn)
            %
            % Wrapped op_addNoise function: Jamie Near, McGill University 2014.
            %
            % USAGE:
            % [out,noise]=op_addNoise(in,sdnoise,noise);
            %
            % DESCRIPTION:
            % Add noise to a spectrum (useful for generating simulated data).  Normally
            % distributed random noise is added to both the real and imaginary parts of
            % the data.  Real and imaginary noise parts are uncorrelated.
            %
            % INPUTS:
            % in         = Input data in matlab structure format.
            % sdnoise    = Standard deviation of the random noise to be added in the time domain.
            % noise      = (optional)  Specific noise kernel to be added (if specified,
            %               sdnoise variable is ignored).
            %
            % OUTPUTS:
            % out        = Output dataset with noise added.
            % noise      = The noise vector that was added.

            if (nargin < 2)
                subspec_filter = [];
            end

            if (nargin < 3)
                error('must specify sdnoise');
            end

            ss = keys(self.processed_subspectra);

            processed = MRS_manipulator.deepish_copy(self.processed_subspectra);

            for si = 1:length(ss)
                ssn = ss{si};

                if ~isempty(subspec_filter) &&~any(strcmp(ssn, subspec_filter))
                    continue;
                end

                f = processed(ssn);
                [ret, ~] = op_addNoise(f, sdnoise);
                processed(ssn) = ret;
            end

            self.processed_subspectra = processed;
            self.push_processed_to_MRS_struct();
        end

        function takeAverages()
            % UNIMPLEMENTED; filtering of averages for current work is handled
            % in MRS_interconnect_FIDA::preprocess
            %
            % At some point, this may become a wrapper for FID-A op_takeaverages.m
            %
            % USAGE:
            % out=op_takeaverages(in,index);
            %
            % DESCRIPTION:
            % Extract the averages with indices corresponding to the 'index' input
            % array.
            %
            % INPUTS:
            % in     = input data in matlab structure format.
            % index  = vector indicating the indices of the averages you would like to extract.
            %
            % OUTPUTS:
            % out    = Output dataset consisting of averages extracted from the input.
            error('takeAverages: not implemented');
        end

    end % }}}

    methods (Static) % {{{

        function res = check_support(silent)
            % check dependencies

            if (nargin == 0) silent = 0; end;
            res = struct('read', 0, 'write', 0, 'import', 0, 'run', 0, 'intermediate', 1);

            % FIDA necessary for export
            if exist('MRS_interconnect_FIDA')
                rf = MRS_interconnect_FIDA.check_support(1);

                if rf.intermediate == 0
                    if (~silent) warning('MRS_manipulator depends on FID-A, which does not appear to be available.'); end;
                else
                    res.write = 1;
                end ;

            end

        end

        function newthing = deepish_copy(thing)
            % deepish_copy non-recursive deep copy of the supplied object
            %
            % For some reason, writing back to the processed_subspectra
            % struct contaminates the parent object. This is
            % surprising. Use this function to instead copy sub-items
            % to a new object. Works with basic struct or
            % containers.Map objects, is adequate for the current
            % application but has not been tested extensively for
            % general use.
            %
            % INPUTS:
            % thing = the object to be copied
            %
            % OUTPUTS:
            % newthing = the new object, with attributes copied

            if isstruct(thing)
                newthing = struct();

                for f = fieldnames(thing)'
                    newthing.(f{1}) = thing.(f{1});
                end

            elseif isa(thing, 'containers.Map')
                newthing = containers.Map();

                for f = keys(thing)
                    newthing(f{1}) = MRS_manipulator.deepish_copy(thing(f{1}));
                end

            else
                warning('Unknown object for deep copy');
                newthing = thing;
            end
        end
    end % methods (Static) }}}
end
