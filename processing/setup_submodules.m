function setup_submodules()
  % Enforce correct versions of the various submodules on path

  if 1 || exist('MRS_interconnect_LCModel')==0
    p=mfilename('fullpath');
    [base,x_,xx_]=fileparts(p);
    [parent,x_,xx_]=fileparts(base);
    submodule_folder=fullfile(parent,'submodules');

    pp=split(path(),':');
    flush_bits={'FID-A','Gannet','MRS_interconnect','osprey','spm'}
    for i=1:length(pp)
      for xi=1:length(flush_bits)
        if ~isempty(strfind(pp{i},flush_bits{xi}))
          disp(sprintf('Removing %s', pp{i}))
          rmpath(pp{i})
          break;
        end
      end
    end

  % add osprey, then force our preferred FID-A over the top
  addpath(genpath(fullfile(submodule_folder,'osprey/libraries'))); 
  addpath(genpath(fullfile(submodule_folder,'osprey/fit')));
  addpath(genpath('/opt/matlab/toolbox/'));

    bits={'FID-A','Gannet3.1','MRS_interconnect','FID-A/inputOutput','FID-A/processingTools', 'FID-A/inputOutput/gannetTools', 'FID-A/processingTools/phase', 'tissue_content_correction/generated','osprey/load','osprey/process','osprey/quantify','osprey/fit','osprey/settings','osprey/GUI', 'osprey', 'osprey/plot', 'osprey/graphics'}
    for bit=bits
      bit=bit{1};
      b=fullfile(submodule_folder,bit);
      if exist(b)
        disp(sprintf('Adding %s', b))
        addpath(b);
      else
        warning(sprintf('Missing %s', b));
      end
    end
  end

  addpath('/opt/simnibs/simnibs-2.1.2-Linux64/resources/spm12');

  addpath(base);

  if ~exist('lsqnonlin')
    error('Gannet 3.1 requires optimization toolbox');
  end
end
