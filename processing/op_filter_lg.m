% op_filter_lg.m
% Line-broadening with adjustable lorentzian/gaussian ratio
% Alexander R Craven, University of Bergen, 2023
% 
% Based on the original (pure Lorentzian) op_filter.m
% Jamie Near, McGill University 2014.
% 
% USAGE:
% [out,lor]=op_filter(in,lb,lg);
% 
% DESCRIPTION:
% Perform line broadening by multiplying the time domain signal by a nominated
% mixture of decay functions (Lorentzian and Gaussian)
% 
% INPUTS:
% in     = input data in matlab structure format.
% lb     = line broadening factor in Hz.
% lg     = lorentz-gauss lineshape param (0.0-1.0)
%          (0: pure lorentz, 1: pure gauss, or mix; default 0)
%
% OUTPUTS:
% out    = Output following alignment of averages.  
% lorgau = Time domain filter envelope that was applied.
%
%
% Gaussian (and lg mix) function derived from the spant implementation 
% https://github.com/martin3141/spant/blob/master/R/mrs_data_proc.R
%
% ...which is consistent with Bottomley, P.A., Griffiths, J.R. (Eds.), 2016.
% Handbook of magnetic resonance spectroscopy in vivo: MRS theory, practice and
% applications, Wiley (Appendix 5.3.1, albeit with "lg" operating in the
% opposite direction to that work's specified Lorentzian fraction, "a")


function [out,lorgau]=op_filter_lg(in,lb,lg);

if nargin<3
  lg=0; % original op_filter behaviour is pure lorentz
end

if lb==0
    out=in;
else
    
    if in.flags.filtered
        %cont=input('WARNING:  Line Broadening has already been performed!  Continue anyway?  (y or n)','s');
        cont='y';
        if cont=='y'
            %continue;
        else
            error('STOPPING');
        end
    end
    
    fids=in.fids;
    
    t2=1/(pi*lb);
    
    lor=1;
    gau=1;

    if lg<1
      %Create an exponential decay (lorentzian filter):
      lor=exp(-(1 - lg) * lb * pi * in.t);
    end

    if lg>0
      % Create a gaussian filter
      if lb>0
        sgn = 1;
      else
        sgn = -1;
      end
      gau=exp(( sgn * lg * ((lb * pi * in.t ).^2))/(4*log(0.5)));
    end

    lorgau=lor.*gau;
    
    %first make a bunch of vectors of ones that are the same lengths as each of
    %the dimensions of the data.  Store them in a cell array for ease of use.
    for n=1:length(in.sz)
        p{n}=ones(in.sz(n),1);
    end

    
    %Now, now take the lorentzian filter vector that we made earlier (lor) and use it
    %to populate a filter array that has the same dimensions as the data.  To
    %do this, we have to use the ndgrid function, which is essentially the same
    %as the meshgrid function, except in multiple dimensions.  b, c, and d are
    %dummy variables and are not used.
    if length(in.sz)==1
        fil=lorgau;
    end
    if length(in.sz)==2
        [fil,b]=ndgrid(lorgau,p{2});
    end
    if length(in.sz)==3
        [fil,b,c]=ndgrid(lorgau,p{2},p{3});
    end
    if length(in.sz)==4
        [fil,b,c,d]=ndgrid(lorgau,p{2},p{3},p{4});
    end
    if length(in.sz)==5
        [fil,b,c,d,e]=ndgrid(lorgau,p{2},p{3},p{4},p{5});
    end
    
    %Now multiply the data by the filter array.
    fids=fids.*fil;
    
    %re-calculate Specs using fft
    specs=fftshift(ifft(fids,[],in.dims.t),in.dims.t);
    
    %FILLING IN DATA STRUCTURE
    out=in;
    out.fids=fids;
    out.specs=specs;
    
    %FILLING IN THE FLAGS
    out.flags=in.flags;
    out.flags.writtentostruct=1;
    out.flags.filtered=1;
    
end
