#!/usr/bin/python3

from glob import glob;
import pandas as pd;
import os;
import sys;
import re;
import numpy as np;

import matplotlib.pyplot as plt;
import matplotlib.patches as mpatches; # for legend creation from fill_between
from scipy import interpolate as interp;
from collections import defaultdict,OrderedDict;
from datetime import datetime;
from matplotlib.lines import Line2D

import big_gaba_common

# QC options {{{
rejection_threshold_corr=0.5;
rejection_threshold_nsd=3;
rejections=defaultdict(list);
# }}} 

include_heatmap=True;
include_individual=False;

extended_range=True;

quick=False;
debug_fslmrs=False;
debug_files=False;
debug_import=[]; # 'lcmodel'];
debug_stdbasis=False;

vendor_map=big_gaba_common.get_vendor_map();

import seaborn as sns;

if include_heatmap:
  import matplotlib.pyplot as plt;

merged=None;
# groupby='site';
groupby=None;

site_folders=glob('/data/big_gaba/[PGS]*');

all_dets=[];
all_specs={};

mode='basic';
ctx=None;
for arg in sys.argv[1:]:
  if ctx is None and arg in ['groupby','mode']:
    ctx=arg;
    continue;
  elif ctx=='groupby':
    groupby=arg;
    ctx=None;
  elif ctx=='mode':
    mode=arg;
    ctx=None;
  elif arg=='quick':
    quick=True;
    include_heatmap=False;
  else:
    raise Exception('Unknown argument: '+arg);

out_folder='output';
if mode!='basic':
  prefix=os.path.join(out_folder,mode);
  if not os.path.exists(prefix):
    os.makedirs(prefix);
else:
  prefix=out_folder;

if quick:
  prefix=os.path.join(prefix,'quick_');
else:
  prefix=os.path.join(prefix,'fit_');

def read_lcm_values(fn): # {{{
  series_type=None;

  series_data={};

  with open(fn) as f:
    vals=[];

    for line in f:
      prev_series_type=series_type;
      if re.match('.*[0-9]+ points on ppm-axis = NY.*',line):
        series_type='ppm';
      elif re.match('.*NY phased data points follow.*',line):
        series_type='data';
      elif re.match('.*NY points of the fit to the data follow.*',line):
        series_type='completeFit'; # completeFit implies baseline+fit
      elif re.match('.*NY background values follow.*',line):
        series_type='baseline';
      elif re.match('.*lines in following.*', line):
        series_type=None;
      elif re.match('[ ]+[a-zA-Z0-9]+[ ]+Conc. = [-+.E0-9]+$', line):
        series_type=None;

      if (prev_series_type!=series_type): # start/end of chunk...
        if len(vals)>0:
          series_data[prev_series_type]=np.array(vals);
          vals=[];
      else:
        if series_type:
          for x in re.finditer(r"([-+.E0-9]+)[ \t]*",line):
            v=x.group(1);
            try:
              v=float(v);
              vals.append(v);
            except:
              print("Error parsing line: %s" % (line,));
              print(v);
  return series_data;
  # }}}

def read_csv_values(fn): # {{{
  series_data={};
  data=pd.read_csv(fn,skipinitialspace=True,index_col=None);
  for c in data.columns:
    series_data[c]=np.array(data[c].values);
  return series_data;
  # }}}

def read_tarquin_fit(fn): # {{{
  series_type=None;

  series_data={};
  data=pd.read_csv(fn,skipinitialspace=True,index_col=None,skiprows=1);

  for c in data.columns:
    series_data[c]=np.array(data[c].values);
  return series_data;

#  }}}

def watermark(ax=None):

  if ax is None:
    ax=plt.gca();

  wm1='DRAFT';
  wm2='Alexander R Craven\n(University of Bergen)\nGenerated %s' % (datetime.now().strftime('%Y-%m-%d %H:%M'),);

  ax.text(0.5, 0.7, wm1, fontsize=50, color='gray', ha='center', va='center', alpha=0.4, transform=ax.transAxes)
  ax.text(0.5, 0.3, wm2, fontsize=20, color='gray', ha='center', va='center', alpha=0.4, transform=ax.transAxes)


if quick:
  lim=200;
  ppm_standard=np.arange(2.6,4.2,.01);
else:
  lim=10000;
  ppm_standard=np.arange(2.6,4.2,.002);

if debug_fslmrs:
  ppm_standard=np.arange(-6,6,.01);

if extended_range:
  ppm_standard=np.arange(0.2,4.2,.01);

ppm_glx=np.arange(3.5,4.0,0.01);
ppm_gaba=np.arange(2.85,3.15,0.01);

out_labels=[];
out_labels_bysite=[];

map_bysite_nosite={};

out_spectra=defaultdict(list);
out_spectra_bysite=defaultdict(list);

out_spectra_backref=defaultdict(list);

persite_permethod=defaultdict(lambda: defaultdict(list));

for site_folder in site_folders: # search and import data {{{
  site=None;
  label=None;
  print(site_folder);
  for root, subdirs, files in os.walk(site_folder):
    b=os.path.basename(root);
    pb=os.path.basename(os.path.dirname(root));
    if pb==site:
      label=b;
    elif re.match('^[PGS][0-9]+_MP$',b):
      site=b;

    if label is not None:
      label_full=label;
    #label=os.path.basename(root);
    #site=os.path.basename(os.path.dirname(root));
    for fn in files:
      # FSLMRS: subfolders
      if fn=='spec.csv':
        m1=re.match('^(?P<id>S[0-9]+_[A-Z]+_68).*[\._](?P<subspec>[a-z0-9]+)$',os.path.basename(root));
      else:
        m1=re.match('^(?P<id>S[0-9]+_[A-Z]+_68).*[\._](?P<subspec>[a-z0-9]+)[\.\_](tarquin.fit|coo|fit.csv)$',fn);

      if not m1:
        m1=re.match('^(?P<id>S[0-9]+)_(?P<subspec>[a-z0-9]+)_(?P<method>[-A-Za-z]+)\.csv$',fn); # jMRUI pattern

      if m1:
        if debug_fslmrs and not 'FSL' in label_full:
          continue;
        print('%s %s %s %s : %s' % (site, label, m1['id'], m1['subspec'],os.path.join(root,fn)));
        if debug_files>1:
          lim=lim-1;
          continue;

        if m1['subspec'] in ['vox1','diff']:
          ss='diff';
        elif m1['subspec']=='osprey':
          ss='diff';
        else:
          ss=None;

        if ss is None:
          continue;

        try:
          label_full=label+'-'+m1['method'];
        except IndexError:
          pass;

        vmap={'GABAGlxData':'data','GABAGlxMetab':'fit','GABAGlxBaseline':'baseline','GABAGlxResid':'resid','PPMScale':'ppm','Data':'data','Fit':'fit','Baseline':'baseline','completeFit':'fit','residual':'resid'};
        vals=None;
        if re.match('.*\.coo$',fn):
          if len(debug_import)>0 and not 'lcmodel' in debug_import:
            continue;
          print('importing LCModel');
          v=read_lcm_values(os.path.join(root,fn));
        elif re.match('.*\.csv$',fn):
          if len(debug_import)>0 and not 'csv' in debug_import:
            continue;
          print('importing CSV %s' % (label_full));
          v=read_csv_values(os.path.join(root,fn));
          if debug_files:
            print(v);
        elif re.match('.*.tarquin.fit$',fn):
          if len(debug_import)>0 and not 'tarquin' in debug_import:
            continue;
          print('importing tarquin_fit');
          v=read_tarquin_fit(os.path.join(root,fn));
        else:
          print('unknown format');
          continue;

        if debug_files:
          lim=lim-1;
          continue;

        sid=m1['id'].split('_')[0];
        persite_permethod[site][label_full].append(sid);

        kk=[k for k in v.keys()];

        for k in kk:
          if k in vmap:
            v[vmap[k]]=v[k];

        if 'data_recreated' in v and not 'data' in v:
          v['data']=v['data_recreated'];

        if 'completeFit' in v and 'baseline' in v: # osprey completeFit also includes the baseline... subtract this for consistency
          v['fit']=v['completeFit']-v['baseline'];

        if 'pred' in v and not 'fit' in v:
          v['fit']=v['pred']-v['baseline']; # FSL-MRS

        if not 'resid' in v:
          if 'baseline' in v:
            v['resid']=v['data']-v['fit']-v['baseline'];
          else:
            v['resid']=v['data']-v['fit'];

        vstd={'ppm':ppm_standard};

        norm_factor=None;
        for k in ['data','fit','resid','baseline']: # NB, ordering determines where norm_factor is determined!
          if k in v:

            delta_h2o=0;
            if 'LCM' in label_full or 'FSLMRS' in label_full or 'Tarquin' in label_full:
              delta_h2o=4.65-4.68;

            ifunc=interp.interp1d(v['ppm']-delta_h2o,v[k],fill_value=0, bounds_error=False);

            x=ifunc(ppm_standard);

            # show only valid range
            if 'LCM' in label_full: # data is also bogus outside of fit range.
              x[np.logical_and(ppm_standard<1.95,ppm_standard>1.2)]=np.nan; # ppmgap
            if k not in ['data']:
              if 'annet' in label_full:
                x[np.logical_or(ppm_standard<2.79,ppm_standard>4.1)]=np.nan;

            if norm_factor is None:
              mx=ifunc(ppm_glx); # normalize to Glx only
              nf_glx=.9/np.percentile(np.abs(mx.flat),95);
              print('Glx norm factor from %s: %.2f' % (k,nf_glx));

              mx=ifunc(ppm_gaba); # normalize to GABA only
              nf_gaba=.55/np.percentile(np.abs(mx.flat),90);
              print('GABA norm factor from %s: %.2f' % (k,nf_gaba));

              # but either individual peak has the (obvious) disadvantage of under-representing variation in that area. thus, take an average of the two...

              norm_factor=(nf_glx+nf_gaba)/2;
              print('Final norm factor from %s: %.2f' % (k,norm_factor));

            vals=norm_factor*x;

            vstd[k]=vals;

            #if groupby=='site':
            out_label_bysite='%s_%s_%s' % (label_full,ss,site);
            out_label='%s_%s' % (label_full,ss);

            out_key='%s_%s' % (out_label, k);
            out_key_bysite='%s_%s' % (out_label_bysite, k);

            out_spectra[out_key].append(vstd[k]);
            out_spectra_bysite[out_key_bysite].append(vstd[k]);

            out_spectra_backref[out_key].append('%s %s' % (site,sid));
            out_spectra_backref[out_key_bysite].append('%s %s' % (site,sid));

            # without spect type (for composite plots...)
            if not out_label in out_labels:
              out_labels.append(out_label);

            if not out_label_bysite in out_labels_bysite:
              out_labels_bysite.append(out_label_bysite);

            map_bysite_nosite[out_key_bysite]=out_key;

            ok='%s_%s_%s_%s' % (label_full,ss,site,sid);
            if not ok in all_specs:
              all_specs[ok]={};
            all_specs[ok][k]=vstd[k];

        dets={'label_full':label_full,'label':label,'subspec':ss,'site':site,'out_label':out_label,'sid':sid, 'key':ok, 'sitesub':'%s_%s' % (site.split('_')[0],sid)};
        if len(debug_import)>0:
          print(dets);
        all_dets.append(dets);

        lim-=1;

  if lim<0:
    break;

  # }}}

if debug_files or len(debug_import)>0:
  raise Exception('Debug: stop');
# now, plot the results:

df=pd.DataFrame(all_dets);
print(df);

print(df.loc[:,['label_full','label']].groupby('label_full').count());

if quick:
  prejects_file=os.path.join(out_folder,'05_gather_fits_rejects_quick.csv');
else:
  prejects_file=os.path.join(out_folder,'05_gather_fits_rejects.csv');


if os.path.exists(prejects_file) and os.path.getmtime(prejects_file)>os.path.getmtime(__file__):
  prejects=pd.read_csv(prejects_file,skipinitialspace=True,sep=';');
  prejects=prejects['sitesub'].unique();
  print(prejects);
else:
  prejects=None;

# plot, all methods combined! {{{

mappings=big_gaba_common.get_mapping(mode);

apply_rejections=True;

pairs=[
  ('FSLMRS-fix-MH-noMM','FSLMRS-fix-MH'),
  ( 'FIDA-Gannet'   ,'Gannet'),
  ( 'jMRUI-AMARES', ),
  ( 'jMRUI-QUEST-noMM',   'jMRUI-QUEST' ),
  ( 'LCModel-noMM',       'LCModel-stdbasis'),
  ( 'Osprey-sep-2021a',   'Osprey-sep-MM3-2021a'), #'Osprey-1to1GABAsoft-bl04'
  ( 'Tarquin-noMM',       'Tarquin-stdbasis'),
  ( 'Tarquin', )
  ];

if mode=='exploratory1':
  pairs=[
  ('Osprey-sep-2021a','Osprey-sep-bl06-2021a','Osprey-sep-MM3-2021a'),                        # a
  ('Osprey-sep-MM3-2021a','Osprey-1to1GABAsoft-bl04-2021a','Osprey-1to1GABAsoft-bl06-2021a'), # b
  ('Osprey-1to1GABAsoft-bl06-2021a','Osprey-3to2MMsoft-bl06-2021a'),                          # c
  ('LCModel-noMM','LCModel-noMM-bl06','LCModel-stdbasis'),                                    # d  
  ('LCModel-stdbasis','LCModel-stdbasis-bl06','LCModel-MM_1to1GABA'),                         # e
  ('LCModel-MM_1to1GABA','LCModel-MM_3to2MM09'), # ,'LCModel-MM_GABA_MM09_ART'),              # f
  ];
elif mode=='exploratory1a':
  pairs=[
  ('Osprey-sep','Osprey-sep-2021a'),  # a
  ('Osprey-sep-bl06','Osprey-sep-bl06-2021a'), # b
  ('Osprey-1to1GABAsoft-bl04','Osprey-sep-MM3-2021a','Osprey-sep-MM3-freeGauss-2021a'), # c
  ('Osprey-1to1GABAsoft-bl04','Osprey-1to1GABAsoft-bl04-2021a'), # d
  ('Osprey-1to1GABAsoft-bl04','Osprey-1to1GABAsoft-bl06'), # e
  ('Osprey-1to1GABAsoft-bl04-2021a','Osprey-1to1GABAsoft-bl06-2021a'), #f
  ('Osprey-1to1GABAsoft-bl04-2021a','Osprey-sep-MM3-2021a'), # g
  ('Osprey-3to2MMsoft-bl06','Osprey-3to2MMsoft-bl06-2021a'), # h
  ('Osprey-sep-MM3-2021a','Osprey-1to1GABAsoft-bl04-2021a','Osprey-1to1GABAsoft-bl06-2021a','Osprey-3to2MMsoft-bl06-2021a'), # i
  ('Osprey-1to1GABAsoft-bl06-2021a','Osprey-3to2MMsoft-bl06-2021a'), # j
  ('LCModel-noMM','Osprey-sep'), # k
  ('LCModel-stdbasis','Osprey-sep-MM3-2021a'), # l
  ('LCModel-MM_1to1GABA','Osprey-1to1GABAsoft-bl06-2021a'), # m
  ('LCModel-MM_3to2MM09','Osprey-3to2MMsoft-bl06-2021a'), # n
  ('LCModel-MM_GABA_MM09_ART','LCModel'),#o
  ('Osprey-sep','Osprey-1to1GABAsoft-bl04'),    # p
  ('Osprey-sep-2021a','Osprey-sep-MM3-2021a')     # q

  ];
  apply_rejections=False;

fill_between_resid=False;
fill_between_fit=False;
fill_between_bl=False;
ax_col=(0.7,0.7,0.7);

if prejects is None and apply_rejections:
  repetitions=['QC','main','byvendor','full','rejections'];
else:
  repetitions=['main','full','byvendor','rejections'];

nice_means=dict();
for rep in repetitions:
  if rep=='full':
    clip_fit=True;
    plot_range=(0.2,4.2);
    ticks=[min(plot_range), 0.9, 2.0, 2.3, 3.02, 3.2, 3.75, max(plot_range)];
  else:
    clip_fit=False;
    plot_range=(2.6,4.2);
    plot_range=(2.7,4.0);
    ticks=[min(plot_range), 3.02, 3.2, 3.75, max(plot_range)];
  tick_labels=['%.2f' % x for x in ticks];

  plot_range_mask=np.logical_and(ppm_standard<=max(plot_range),ppm_standard>min(plot_range));

  grouped_spectra={};

  resid_col=(rep not in ['full','byvendor']);

  figure_width=big_gaba_common.get_figure_widths();

  if rep=='QC':
    fig=None;
    axes=None;
  else:
    if rep=='byvendor':
      nc=3;
    elif resid_col:
      nc=2;
    else:
      nc=1;
    if rep=='full':
      hp=-8
    else:
      hp=-4;

    method_legend_hpos=0.9;
    if rep=='main':
      fw=figure_width['1.5col'];
      main_lcols=1;
      main_legend_hpos=.3;
    else:
      fw=figure_width['full'];
      if rep=='byvendor':
        main_lcols=1;
        method_legend_hpos=.84;
      else:
        main_lcols=2;
      main_legend_hpos=.33;
      if rep=='full':
        main_legend_hpos=.55;
        method_legend_hpos=.87;


    fig,axes=plt.subplots(nrows=len(pairs),ncols=nc,sharex=False,sharey='col',figsize=(fw,6),dpi=300,squeeze=False,);
    if 'exploratory' in mode and rep=='full':
      # in full exploratory, need some extra space to the right for the legend text
      fig.tight_layout(h_pad=hp,w_pad=.1, rect=(.02,.02,.95,1),pad=.2); # slightly reduce outer rectangle so our bottom/left axis labels show
    else:
      fig.tight_layout(h_pad=hp,w_pad=.1, rect=(.02,.02,1,1),pad=.2); # slightly reduce outer rectangle so our bottom/left axis labels show

  for pair_ix in range(len(pairs)): # each pair goes to a new row... {{{
    add_common_dashed=False;
    is_lastrow=(pair_ix==len(pairs)-1);
    is_firstrow=(pair_ix==0);
    group=pairs[pair_ix];
    last_bl=None;
    last_resid=None;
    last_fit=None;
    last_col=None;
    name_basic=None;

    axcol=-1;

    if rep=='byvendor':
      sitefilters=['G','P','S']
    else:
      sitefilters=[None];

    for sitefilter in sitefilters: # {{{

      axcol=axcol+1;

      if axes is not None:
        ax_f=axes[pair_ix,axcol];
        if resid_col:
          ax_r=axes[pair_ix,axcol+1];
        else:
          ax_r=None;

      for group_ix in range(len(group)): # for each group on this axis... {{{
        if group_ix>0 and rep not in ['main','full','byvendor']:
          continue;

        manual_legend_indiv=[];
        gk=group[group_ix];
        this_group=df.loc[df['label_full']==gk,:];

        if sitefilter is not None:
          gkv=gk+sitefilter;
          this_group=this_group.loc[[x[0]==sitefilter for x in this_group['site']],:];
        else:
          gkv=gk;

        if (rep!='QC') and (prejects is not None):
          preject_mask=[x not in prejects for x in this_group['sitesub']];
          if rep=='rejections':
            preject_mask=[not x for x in preject_mask];
          print(prejects);
          print(preject_mask);
          this_group=this_group.loc[preject_mask,:];
        print('%s : %s : %d' % (gk, gkv, len(this_group)))
        gkeys=list(set(this_group['key']));
        print(gkeys);
        N=len(gkeys);
        ix=0;
        print(grouped_spectra);

        for k in gkeys:
          if not gkv in grouped_spectra:
            grouped_spectra[gkv]={};
          for part in all_specs[k].keys():
            if not part in grouped_spectra[gkv]:
              grouped_spectra[gkv][part]=np.zeros((N,len(ppm_standard)));
            grouped_spectra[gkv][part][ix,:]=all_specs[k][part];
          ix=ix+1;

        # build the group....

        if rep=='QC':
          if (prejects is None) and (group_ix==0): # do correlations for this group? {{{
            mcorr=dict();
            corr_range_mask=np.logical_and(ppm_standard>2.6,ppm_standard<4.2);
            for i in range(N):
              mask=np.ones((N,));
              mask[i]=0;

              if corr_range_mask is None:
                sel=grouped_spectra[gkv]['data'][i,:];
                sub=grouped_spectra[gkv]['data'][mask>0,:];
              else:
                rangemasked=grouped_spectra[gkv]['data'][:,corr_range_mask];
                sel=rangemasked[i,:];
                sub=rangemasked[mask>0,:];

              sub_mean=sub.mean(axis=0);

              # normalize...
              sel_norm=sel/np.sqrt((np.sum(np.square(sel))));
              sub_mean_norm=sub_mean/np.sqrt((np.sum(np.square(sub_mean))));

              mcorr[gkeys[i]]=np.correlate(sel_norm,sub_mean_norm)[0];

            kk=[];
            vv=[];
            for k,v in mcorr.items():
              kk.append(k);
              vv.append(v);
            vv=np.array(vv);
            vstd=np.std(vv);
            vmean=np.mean(vv);
            vx=np.abs(vv-vmean)/vstd;

            print(mcorr);

            for k,v,vx in zip(kk,vv,vx):
              if (quick and mode=='basic' and np.random.randint(100)>70) or (vx>rejection_threshold_nsd) or (v<rejection_threshold_corr):
                print('REJECT %s : %.2f : %.2f' % (k,v,vx));
                dets=df.loc[df['key']==k,:].to_dict('records')[0];
                dets['data_corr']=v;
                dets['data_corr_nsd']=vx;
                print(dets);
                rejections[k]=dets; # .append('%s:%.2f/%.2f' % (k,v,vx));
                #mask[ik_to_ix_map[k]]=0;

          # }}}
          continue;

        if gkv in grouped_spectra:

          show_individual=(rep=='rejections');
          if gk in mappings:
            name_nice=mappings[gk];
          else:
            name_nice=gk;
          name_short=name_nice;

          if name_basic is None:
            name_basic=name_nice;
          elif False and 'MM3' in name_nice and name_basic in name_nice:
            name_short=re.sub('^%s[ -]*' % name_basic,'', re.sub('[()]','',name_nice));

          print('%s : %s' % (name_nice,name_short));


          ppm_plot=ppm_standard[plot_range_mask];

          print(grouped_spectra[gkv].keys());

          cp=sns.color_palette(palette=None,n_colors=6); #,desat=

          if mode=='exploratory1a':
            lc=cp[group_ix];
          elif mode=='exploratory1':
            if gk in ['Osprey-sep-2021a','LCModel-noMM']:
              lc=cp[0];
            elif gk in ['LCModel-stdbasis','Osprey-sep-MM3-2021a']:
              lc=cp[1];
            elif '1to1GABAsoft-bl04' in gk:
              lc=cp[4];
            elif '3to2MM' in gk:
              lc=cp[2];
            elif '1to1GABA' in gk:
              lc=cp[3];
            else:
              lc=cp[5];

          elif mode=='exploratory1' and ('Osprey' in gk and not '2021a' in gk):
            lc=cp[5];
          elif gk in ['Gannet','Tarquin'] or ('3to2MM' in gk):
            lc=(.6,0,.6); # "native" methods
            lc=cp[3];
          elif ('Gannet' in gk or 'AMARES' in gk) or ('1to1GABA' in gk):
            lc=(0,.5,0);  # peak-fitting
            lc=cp[2];
          elif 'noMM' in gk or gk in ['Osprey-sep','Osprey-sep-2021a']:
            lc=(1,.4,0);
            lc=cp[0];
          elif mode=='exploratory1':
            if gk in ['LCModel-MM_GABA_MM09_ART']:
              lc=cp[4];
            elif 'LCModel' in gk:
              lc=cp[5];
            else:
              lc=cp[1];
          else:
            lc=(0,.4,1);
            lc=cp[1];

          # show data in background
          if group_ix==0:
            if rep=='rejections':
              d=nice_means[gkv+'data'];
              d_sd=nice_means[gkv+'data_sd'];
            else:
              d=np.nanmean(grouped_spectra[gkv]['data'],axis=0)[plot_range_mask];
              d_sd=np.nanstd(grouped_spectra[gkv]['data'],axis=0)[plot_range_mask];
              if rep=='main':
                nice_means[gkv+'data']=d;
                nice_means[gkv+'data_sd']=d_sd;


            # 0066ff, .27 blue-ish
            # ff66 orange

            # 0055d4 or 71c837 or 008000 (bg)
            # 00d400?

            # color?
            col_d=[0.2,0.2,0.2];
            col_d_alpha=1.0;
            #if rep in ['main','full']:
            #  col_d_alpha=0.8;
            #else:

            col_sd=[.5,.5,.7];
            col_sd_alpha=.2;
            if not show_individual:
              h_d_sd=ax_f.fill_between(ppm_plot,d-d_sd,d+d_sd,alpha=col_sd_alpha,color=col_sd,);
            h_d,=ax_f.plot(ppm_plot,d,color=col_d,alpha=col_d_alpha,lw=1);


            if False:
              for ii in range(N):
                checkrange=np.logical_and(ppm_standard<4.0,ppm_standard>3.5);
                checkglx=grouped_spectra[gk]['data'][ii,checkrange];
                print('%d : %.2f' % (ii,np.nanmean(np.abs(checkglx))));
                #mx=np.array(ifunc(ppm_glx)); # normalize to Glx only
                #    norm_factor=.9/np.percentile(np.abs(mx.flat),95);
                ax_f.plot(ppm_plot,grouped_spectra[gkv]['data'][ii,plot_range_mask]); #,label=gkeys[ii]);

          if rep=='rejections': # plot individual traces... {{{
            ialpha=0.4;
            ialpha_d=ialpha/3;
            for ii in range(N):
              indiv_d=grouped_spectra[gk]['data'][ii,plot_range_mask];
              indiv_f=grouped_spectra[gk]['fit'][ii,plot_range_mask];

              if 'baseline' in grouped_spectra[gk]:
                indiv_b=grouped_spectra[gk]['baseline'][ii,plot_range_mask];
              else:
                indiv_b=np.zeros((sum(plot_range_mask),));

              indiv_r=grouped_spectra[gk]['resid'][ii,plot_range_mask];

              # representative_handle* used for legend generation

              if group_ix==0:
                representative_handle_d,=ax_f.plot(ppm_plot,indiv_d,         alpha=ialpha_d, color=(0,0,0),lw=1,zorder=1,clip_on=clip_fit);
                #if ii=0:
                #  manual_legend_indiv.append((h,name_short));


              h,=ax_f.plot(ppm_plot,indiv_f+indiv_b, alpha=ialpha, color=lc,       lw=1,zorder=10,clip_on=clip_fit);
              if ii==0:
                manual_legend_indiv.append((h,name_short));

              if 'baseline' in grouped_spectra[gk]:
                h,=ax_f.plot(ppm_plot,indiv_b, alpha=ialpha, color=lc,       linestyle='--',lw=1,zorder=2,clip_on=clip_fit);
                if ii==0 and len(pairs)<9 and not 'exploratory' in mode: # if there are way too many rows, hide baseline from legend so at least the main line shows up clearly
                  manual_legend_indiv.append((h,'(baseline)'));
              else:
                representative_handle_bl=None;
              if resid_col:
                ax_r.plot(ppm_plot,indiv_r,         alpha=ialpha, color=lc,       lw=1,zorder=5,clip_on=clip_fit);

          # }}}

          m=np.nanmean(grouped_spectra[gkv]['fit'],axis=0);
          r=np.nanmean(grouped_spectra[gkv]['resid'],axis=0);

          if not show_individual: # group mean plots {{{

            if 'baseline' in grouped_spectra[gkv]:
              bl=np.nanmean(grouped_spectra[gkv]['baseline'],axis=0);
              if fill_between_bl and last_bl is not None:
                ax_f.fill_between(ppm_standard[plot_range_mask],bl[plot_range_mask],last_bl[plot_range_mask],color=last_col,zorder=0,alpha=0.2);
            else:
              bl=np.zeros((len(ppm_standard),));
            last_bl=bl;

            if fill_between_fit and last_fit is not None:
              ax_f.fill_between(ppm_standard[plot_range_mask],bl[plot_range_mask]+m[plot_range_mask],last_fit[plot_range_mask],color=last_col,zorder=0,alpha=0.2);
            last_fit=m+bl;

            ax_f.plot(ppm_standard[plot_range_mask],m[plot_range_mask]+bl[plot_range_mask],label=name_short,color=lc,lw=1.2,zorder=5,alpha=0.8,clip_on=clip_fit);

            # we want baseline below the main fit, but we also want it after in the legend. plot according to legend order, then use z-order to manipulate layering
            if 'baseline' in grouped_spectra[gkv]:
              if len(pairs)<9 and not 'exploratory' in mode:
                label='(baseline)';
              else:
                add_common_dashed=True;
                label=None;
              ax_f.plot(ppm_standard[plot_range_mask],bl[plot_range_mask],label=label,zorder=3,color=lc,linestyle='--',lw=1);

            if resid_col:
              if fill_between_resid and last_resid is not None:
                ax_r.fill_between(ppm_standard[plot_range_mask],last_resid[plot_range_mask],r[plot_range_mask],color=last_col,alpha=0.2);
            last_resid=r;

            if resid_col:
              ax_r.plot(ppm_standard[plot_range_mask],r[plot_range_mask],lw=1.2,color=lc,alpha=0.8,zorder=5,clip_on=False);

            # }}}

          if group_ix==0 and is_lastrow:
            #ax_f.axhline(y=0,color=(.8,.8,.8),lw=1);
            #ax_r.axhline(y=0,color=(.8,.8,.8),lw=1);
            ax_f.plot([min(ticks),max(ticks)],[0,0],color=(.8,.8,.8),lw=1,zorder=0);
            if resid_col:
              ax_r.plot([min(ticks),max(ticks)],[0,0],color=(.8,.8,.8),lw=1,zorder=0);

          last_col=lc;

    # Doesn't work with sharex?

      # groups }}}

      if rep=='QC':
        break;

      spad=.03;
      ax_f.set_xlim(min(ticks)-spad,max(ticks)+spad);
      if resid_col:
        ax_r.set_xlim(min(ticks)-spad,max(ticks)+spad);
      ax_f.invert_xaxis();
      if resid_col:
        ax_r.invert_xaxis();
      #ax_r.patch.set_facecolor(None);
      ax_f.patch.set_alpha(0.0);
      if resid_col:
        ax_r.patch.set_alpha(0.0);

      legend_vpos=.77; # .75 looks nice, but 'osprey' baseline gets lost :(

      l2=None;

      if is_firstrow:
        if rep=='rejections':
          # leading space to nudge title to the right of the Glx peaks
          ax_f.set_title('    Fits to Rejected Spectra',     loc='center',y=1,pad=-9,fontdict={'fontsize':'small','fontweight':'bold'},color='red');
          if resid_col:
            ax_r.set_title('Corresponding Residuals',loc='center',y=1,pad=-9,fontdict={'fontsize':'small','fontweight':'bold'},color='red');

          try:
            l2=ax_f.legend(bbox_to_anchor=(0.33,legend_vpos),loc='upper left',fontsize='xx-small',borderaxespad=0.,frameon=False, handles=[h_d,representative_handle_d], labels=['Processed signal (mean non-rejected)','Rejected spectra'], ncol=1);
          except NameError:
            # no rejects, this might fail
            pass;
        else:
          if rep=='byvendor':
            ax_f.set_title('Mean %s (N=%d)' % (vendor_map[sitefilter],len(this_group)),     loc='right',y=1,pad=-9,fontdict={'fontsize':'small','fontweight':'bold'});
          else:
            if rep=='full':
              nudge='          '; # centered title overlaps with the 2.3ppm complex. add some space to nudge it to the right.
            else:
              nudge='';
            ax_f.set_title(nudge+'Mean Fit',     loc='center',y=1,pad=-9,fontdict={'fontsize':'small','fontweight':'bold'});
          if resid_col:
            ax_r.set_title('Mean Residual',loc='center',y=1,pad=-9,fontdict={'fontsize':'small','fontweight':'bold'});

          h_sd_patch=mpatches.Patch(color=col_sd, label='+/- 1 SD',alpha=col_sd_alpha)
          l2=ax_f.legend(bbox_to_anchor=(main_legend_hpos,legend_vpos),loc='upper left',fontsize='xx-small',borderaxespad=0.,frameon=False, handles=[h_d,h_sd_patch], labels=['Processed signal','+/- 1 SD'], ncol=main_lcols);

      otherbits={'frameon':False}
      if show_individual:
        #legend_bits=[(name_short,representative_handle_fit),('(baseline)',representative_handle_bl)];
        legend_handles=[h for h,l in manual_legend_indiv];
        legend_labels= [l for h,l in manual_legend_indiv];
        #if representative_handle_bl is None:
      else:
        legend_handles, legend_labels = ax_f.get_legend_handles_labels()
        otherbits={'frameon':False}
        if rep=='full':
          otherbits={'edgecolor':'none'};
        if add_common_dashed:
          legend_handles.append( Line2D([0], [0], marker=None, linestyle='--',color=(.5,.5,.5), linewidth=1));
          legend_labels.append('(corresponding baselines)');

      if axcol<2:
        ax_f.legend(handles=legend_handles,labels=legend_labels, bbox_to_anchor=(method_legend_hpos, legend_vpos), loc='upper left', borderaxespad=0.,fontsize='xx-small', **otherbits).set_zorder(10);
        l=ax_f.get_legend();
      else:
        l=None;

      if is_firstrow:
        if l2 is not None:
          ax_f.add_artist(l2);
      #ax_f.legend(bbox_to_anchor=(1.05, .8), loc='upper center', borderaxespad=0.,fontsize='xx-small',ncol=len(group))

      if axcol<=0:
        #subpart_label='%c) %s' % (ord('a')+pair_ix,name_basic);
        subpart_label='%c)' % (ord('a')+pair_ix,);
        spl_hpos=-.1;
        spl_vpos=legend_vpos-.017;
        if rep=='full':
          spl_hpos=-.046;
          spl_vpos=0.82;
        ax_f.text(spl_hpos,spl_vpos,subpart_label,fontsize='x-small',fontweight='bold',color=(0,0,0),transform=ax_f.transAxes,zorder=20,verticalalignment='top');

      # enbolden the first item in the legend (this would be the basic method)
      try:
        if l is not None:
          t=list(l.get_texts())[0];
          fp=t.get_fontproperties().copy();
          fp.set_weight('bold');
          t.set_fontproperties(fp);
      except IndexError:
        pass;

      ax_f.spines['top'].set_visible(False)
      ax_f.spines['right'].set_visible(False)
      # except the last:
      if rep=='full':
        ax_f.set_ylim([-2,1.5]);
      else:
        ax_f.set_ylim([-0.2,1.1]);
      #ax_r.set_ylim([-0.2,0.2]);

      if resid_col:
        ax_r.set_ylim([-0.3,0.7]);
        ax_r.spines['top'].set_visible(False)

      # lower spine/axis
      # ticks=[min(plot_range), 3.02, 3.2, 3.72, 3.79, max(plot_range)];


      ax_f.set_xticks(ticks);

      ax_f.set_yticks([0,.5]);
      ax_f.set_yticklabels(['0','50%'],fontsize='xx-small');
      ax_f.spines['left'].set_bounds(0,.5)
      ax_f.spines['left'].set_color(ax_col);

      if resid_col:
        ax_r.set_xticks(ticks);

        ax_r.set_yticks([-.25,0,.25]);
        ax_r.set_yticklabels(['-25%','','+25%'],fontsize='xx-small');

        ax_r.tick_params(axis="y",direction="in", pad=-22)
        ax_r.spines['right'].set_bounds(-.25,.25);
        ax_r.spines['right'].set_color(ax_col);
        ax_r.spines['left'].set_visible(False)

        ax_r.yaxis.set_label_position("right")
        ax_r.yaxis.tick_right()


      #ax.spines['top'].set_color('red')
      #ax.xaxis.label.set_color('red')
      ax_f.tick_params(axis='y', colors=ax_col);
      ax_f.spines['bottom'].set_bounds(min(ticks),max(ticks));

      if resid_col:
        ax_r.tick_params(axis='y', colors=ax_col);
        ax_r.spines['bottom'].set_bounds(min(ticks),max(ticks));

      #ax_f.tick_params(fontsize='xx-small');
      if not is_lastrow:
        ax_f.set_xticklabels([]);
        if resid_col:
          ax_r.set_xticklabels([]);
        # set_position('zero' despite visible(False): move ticks up to the zero line
        ax_f.spines['bottom'].set_position('zero')
        ax_f.spines['bottom'].set_color(ax_col);
        ax_f.tick_params(axis='x', colors=ax_col);

        if resid_col:
          ax_r.spines['bottom'].set_position('zero')
          ax_r.spines['bottom'].set_color(ax_col);
          ax_r.tick_params(axis='x', colors=ax_col);
        #ax_f.spines['bottom'].set_visible(False)
       # ax_r.spines['bottom'].set_visible(False)
      else:

        #print(h_d_sd.legend_elements());
        #loc='lower right')
        #ax_f.add_artist(l2);

        ax_f.set_xticklabels(tick_labels,fontsize='xx-small');
        if resid_col:
          ax_r.set_xticklabels(tick_labels,fontsize='xx-small');
        ax_f.set_xlabel('Chemical Shift (ppm)',fontsize='x-small');
        if resid_col:
          ax_r.set_xlabel('Chemical Shift (ppm)',fontsize='x-small');

        #ax_f.get_xaxis().set_visible(False)    # ticks etc
        #ax_r.get_xaxis().set_visible(False)

    # sitefilter }}}

  # }}}
  
  if rep=='QC':
    print('==== REJECTIONS ====');
    print(rejections);
    if len(rejections)>0:
      dfr=pd.DataFrame([i for k,i in rejections.items()]);
      print(dfr);
      dfr_interesting=dfr.loc[dfr['label_full']=='FIDA-Gannet',['label_full','data_corr','data_corr_nsd','sitesub']];
      print(dfr_interesting.groupby(['sitesub']).count());
      print(dfr.columns);
      dfr_interesting.to_csv(prejects_file,sep=';',index=True,float_format='%.3G');
      prejects=dfr_interesting['sitesub'].unique();
    elif apply_rejections:
      prejects=[];
    print(prejects);

    # plot reject/accept figure! {{{

    # now, R1 axes are 6x1.392", but that's just the plot area, figure is a bit bigger. 7.5x1.6ish? (measured externally, from svg, in inkscape)
    f2,ax2=plt.subplots(nrows=1,ncols=1,sharex=False,sharey='col',figsize=(7.5,1.7),dpi=300,squeeze=False,);
    f2.tight_layout(rect=(.02,.05,1,1),pad=.2); # slightly reduce outer rectangle so our bottom/left axis labels show
    gk='FIDA-Gannet';
    this_group=df.loc[df['label_full']==gk,:];

    if apply_rejections:
      preject_mask=[x not in prejects for x in this_group['sitesub']];
    else:
      preject_mask=[];
    accept=this_group.loc[preject_mask,:];
    reject=this_group.loc[[not x for x in preject_mask],:];
    gkeys_accept=list(set(accept['key']));
    gkeys_reject=list(set(reject['key']));
    ax=ax2[0,0];

    for k in gkeys_accept:
      data=all_specs[k]['data'];
      h_accept,=ax.plot(ppm_standard,data,color=(0.3,0.3,0.3),alpha=0.07,lw=1);

    for k in gkeys_reject:
      data=all_specs[k]['data'];
      h_reject,=ax.plot(ppm_standard,data,color=(.8,0,0),alpha=0.4,lw=1);
    ax.set_ylim([-2,1.5]);
    ax.invert_xaxis();


    try:
      ax.legend(loc='lower right',fontsize='xx-small', framealpha=0.9, handles=[h_accept,h_reject], labels=['Accept by R1 (N=%d)' % (len(gkeys_accept),),'Reject by R1 (N=%d)' % (len(gkeys_reject),)], ncol=1);
    except NameError:
      # h_reject might be undefined if no rejects?
      pass;

    ax.set_yticks([0,1]);
    ax.set_yticklabels(['0','100%'],fontsize='xx-small');
    ax.spines['left'].set_bounds(0,1)
    ax.spines['left'].set_color(ax_col);
    ax.tick_params(axis='y', colors=ax_col);

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ticks=[min(ppm_standard), 0.9, 2.0, 2.3, 3.0, 3.75, max(ppm_standard)];
    tick_labels=['%.1f' % x for x in ticks];

    ax.set_xticks(ticks);
    ax.set_xticklabels(tick_labels,fontsize='xx-small');
    ax.spines['bottom'].set_bounds(min(ticks),max(ticks));
    ax.set_xlabel('Chemical Shift (ppm)',fontsize='xx-small');

    big_gaba_common.savefig(prefix+'R1.png',fig=f2);
    plt.close(f2);

    # }}}

    continue;
  else:
    big_gaba_common.savefig(prefix+rep+'.png',fig=fig);
    plt.close(fig);
    print(prefix+rep+'.png');

  for site in persite_permethod:
    for label in persite_permethod[site]:
      print('%6s %20s %s' % (site,label,str(persite_permethod[site][label])));


# }}}

print(prejects)
