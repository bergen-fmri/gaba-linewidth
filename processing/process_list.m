function ret=process_list(subs)
  % process_list : perform defined processing on each subject in supplied list (subs)
  %
  % This has several modes of operation:
  %
  % MS1 : basic quantification, without application of SNR/LB manipulations.
  %       This reflects the original analysis performed in
  %       https://doi.org/10.1002/nbm.4702
  % MS2 : quantification with SNR/LB manipulations (currently under review)
  % MS2sim : quantification with SNR/LB manipulations, for in-vivo-like simulated data
  % MS3 : future work in progress
  %
  % The reader may notice "jMRUI" appears in several places, despite that
  % software not being reported in the current analysis. This is for historical
  % reasons:
  % 
  % - In the previous MS1 analysis, the jMRUI module was the first to be run
  % - Although jMRUI itself is not used in the present analysis, its data
  %   format is used as an intermediary for some of the other algorithms which
  %   are reported here, and for caching/continuity across runs
  % - Our jMRUI module subclasses our FIDA module, hence provides access to
  %   FIDA preprocessing (including, significantly, subsampling transients for
  %   reduced SNR subsets of the incoming data)


  mode='MS2';

  quick=0; % use a reduced subset of parameters and fast variants of selected algorithms -- for quick debugging
  continuation=0; % resume previous analysis (load cached data to ensure same random sampling)
  dry=0; % dry run: iterate over loops, but don't actually fit or write result data

  stop_on_error=quick; % in debug mode, stop at the first error we hit. in batch mode, keep going
  enable_caching=0;
  enable_manipulations=0;

  include_noMM=0;

  % SNR match by additive noise for subsets larget than this... NOT REPORTED in the current work
  % <0 : don't do SNR match
  do_snr_match_for_subsets=-1; .99;

  % Line broadening function, 0.0: pure lorentz (old default), 1.0 pure gauss (added 20231021)
  lb_lor_gau=0.0;

  % which subspectra are subject to line broadening? (nb, broadened "ref" NOT REPORTED)
  lb_parts={'diff','off'}

  if (strncmp(mode,'MS1',3))
    output_prefix=[];
    subset_options=[];
    lb_options=[];
    enable_caching=1;
    include_noMM=1;
  elseif (strcmp(mode,'MS2')) % MS2 applies SNR and LB manipulations; this is the in-vivo part.
    output_prefix='manip';
    enable_manipulations=1;

    % quantification on random reduced subset of available transients (emulate low-SNR cases)
    subset_options_full=[1 .5 .25 .125 .0625 .03125]; 
    subset_options=subset_options_full; % can be reduced in certain modes
    subset_blocksize=2; % retain edit ON/OFF pairs

    % linebroadening, in Hz; lorentz/gauss factor determined above in lb_lor_gau
    lb_options=unique(horzcat([0:.2:2],[2:.5:5],[5:1:10]));
    if quick 
      if quick_with_full_range<0
        subset_options=[1];
      elseif quick_with_full_range < 1
        subset_options=[1 .5 .0625];
        lb_options=[3 0 0.6 ];
      end
    end
  elseif (strcmp(mode,'MS2sim')) % MS2 applies SNR and LB manipulations; this is the in-vivo part.
    output_prefix='simulated';
    enable_manipulations=1;
    subset_options=[];
    include_simple_integral=1
    % see MRS_Overlay.m calib mode {{{

    % simulated linewidths
    lb_options=[2:.5:12];

    % simulated GABA/GABA+ concentrations
    conc_options=[2.5,3.2,3.9];

    % simulated models (see report section 2.3)
    % simmode==0 : G     : Pure GABA only (no other metabs); some algos need special treatment
    % simmode==1 : GM    : Pure GABA + other metabs
    % simmode==2 : -     : GABA+MM3 (50% split) : NOT REPORTED
    % simmode==3 : G+M   : GABA, fixed 1.5mmol MM3
    % simmode==4 : GMB   : Pure GABA, undulating baseline
    % simmode==5 : GMR   : Pure GABA, residuals (noise/features)
    % simmode==6 : GMBR  : Pure GABA, full background (undulating baseline, residual noise/features)
    % simmode==7 : G+MBR : GABA, fixed 1.5mmol MM3, full background
    simmode_options=0:7;

    % }}}
    if quick && ~quick_with_full_range
      lb_options=[2 10 ];
    end
  else
    error('Unknown mode');
  end

  v=version('-release');
  year=str2num(v(1:4));
  if (year<2015)
    % datetime function, introduced in 2014b, is required for the siemens loader
    error('Your MATLAB release is quite ancient; this might not work out particularly well.');
  end

  [folder,~,~]=fileparts(mfilename('fullpath'));
  [~,pb,pe]=fileparts(folder);

  is_ms2=strcmp(mode,'MS2');
  is_ms2sim=strcmp(mode,'MS2sim');
  is_ms3=strcmp(mode,'MS3');

  if ~is_ms3
    setup_submodules();
  end

  output_per_site=1;
  output_per_sub=is_ms2||is_ms2sim||is_ms3;

  include_gannet_preproc=is_ms3; % XX ??
  include_fida_preproc=~is_ms3;

  include_bl_exploration=0;
  include_mm_exploration=0;
  extended_mm_exploration=0;

  include_simple_integral=0
  include_lcmodel_flat=0

  include_osprey_separate=~is_ms3;

  if include_mm_exploration
    include_osprey_separate_MM_1to1GABA=1;
    include_osprey_separate_MM_3to2MM09=1;
    include_lcmodel_MM_3to2MM09=1;
    include_lcmodel_MM_GABA_MM09_ART=1;
  else
    include_osprey_separate_MM_1to1GABA=0;
    include_osprey_separate_MM_3to2MM09=0;
    include_lcmodel_MM_3to2MM09=0;
    include_lcmodel_MM_GABA_MM09_ART=0;
  end

  include_osprey_concat=0;                % XX
  include_osprey_fida_preproc=~is_ms3;          % XX
  include_osprey_native_preproc=0;        % XX
  include_osprey_fida_preproc_concat=0;   % XX

  include_fsl_mrs_noMM_MH=0;

  include_tarquin=1;
  include_gannet=1;
  include_lcmodel=1;
  include_jmrui=1; %  also needed for quality!
  include_osprey=0; 
  include_fsl_mrs=1;

  if quick
    include_fsl_mrs_noMM_MH=0;
    include_fsl_mrs_MH=0;
    include_fsl_mrs_Newton=1;
    include_tarquin_stdbasis_off=0; % possibly gets hung up on some poor-quality spectra
    include_osprey=0; % osprey is not particularly quick.
  else
    include_tarquin_stdbasis_off=1; 
    include_fsl_mrs_MH=1;
    include_fsl_mrs_Newton=0;
  end

  if is_ms2sim
    assert(logical(continuation),'ms2sim must be run with "continuation" set: all simulation is performed externally, so processed data must be imported here.');
    include_gannet=0; % this is already run during the original simulation
  end

  if continuation && (include_jmrui||(include_lcmodel&&~is_ms2sim))
    error('include_jmrui/include_lcmodel is not compatible with "continuation", which needs to read previous raw data from one of those')
  end

  include_fsl_mrs_Newton_for_off=1;
  include_lcmodel_fancy=0;
  include_lcmodel_universal=0;

  % these use standard basis-set
  include_lcmodel_noMM=include_noMM;
  include_lcmodel_MM_1to1GABA=0;

  include_fsl_mrs_noMM=include_noMM;

  include_fsl_mrs_MH=1;
  include_fsl_mrs_Grouped=0;

  include_tarquin_noMM=include_noMM;

  include_standard_basis=1;


  all_results=datatable_ARC();
  dn=pwd

  output_filename_base=fullfile(dn,sprintf('output.%s', datestr(now,'yyyymmdd.HHMM')))

  owd=pwd;
  last_site=[];

  for i=1:length(subs) % for each subject.. {{{
    close all;

    % Some of our submodules leak filehandles.
    % Osprey LogFile handles are not closed reliably
    % Philips SPAR files (and possibly others are not closed reliably by Gannet
    % Just close everything which is still open:
    x=fclose('all');

    figHandles = findall(groot, 'Type', 'figure');

    if numel(figHandles)>0
      warning('Some figure handles were not closed correctly.');
    end

    ret=[];
    java.lang.System.gc();

    % and yet, something leaks memory.
    % osprey/libraries/L-BFGS-B-C/Matlab/lbfgsb.m has persistent things. is it that?
    % https://www.ibm.com/developerworks/library/j-jtp01274/

    this_set=subs{i}

    if iscell(this_set)
      this_main=this_set{1}
    else
      this_main=this_set
    end

    if include_osprey
      % Osprey adjusts the matlab path, and forces its version of FID-A upon us. Regain control of this...
      force_FIDA('compat'); % use standard upstream (ish) FID-A
    end

    LCM_results=[];
    [mb,mn,me]=fileparts(this_main);
    [site_folder,subject_name,mbe]=fileparts(mb);      % mbb: site folder, mbn: subject name

    [mbbb,site_name,mbbe]=fileparts(site_folder);  % mbbb: top-level folder; mbbn: site folder name

    if ~isempty(output_prefix)
      output_base=fullfile(mbbb,output_prefix);
      site_folder=fullfile(output_base,site_name);

      if ~exist(site_folder,'dir')
        if ~exist(output_base,'dir')
          mkdir(output_base);
        end
        mkdir(site_folder);
      end
    end

    cd(site_folder);

    if output_per_sub>0

      sub_folder=fullfile(site_folder,subject_name);
      if ~exist(sub_folder,'dir')
        mkdir(sub_folder);
      end

      all_results=datatable_ARC();
      output_filename_base=fullfile(sub_folder,sprintf('output.%s', datestr(now,'yyyymmdd.HHMM')));

    elseif output_per_site>0
    if isempty(last_site) || ~strcmp(site_name,last_site) % new site
        all_results=datatable_ARC();
        output_filename_base=fullfile(site_folder,sprintf('output.%s', datestr(now,'yyyymmdd.HHMM')))
        last_site=site_name;
      end
    end

    site_name
    switch(site_name(1))
      case 'G'
        std_basis_vendor='ge';
      case 'P'
        std_basis_vendor='philips';
      case 'S'
        std_basis_vendor='siemens';
      otherwise
        error('Unrecognised vendor')
    end

    std_basis_vendor

    % result table can be cumbersome for multiple nested loops; optionally split across several files
    output_filename_inc=1;
    output_filename=sprintf('%s-%03d.csv', output_filename_base, output_filename_inc);
    %%%%% Initial run : Gannet for loading
    try % process this subject within a catch-all exception handing block {{{

      sprintf('Processing: "%s"',this_main)

      jobs={};

      if include_gannet || include_jmrui || include_fida_preproc || include_osprey_fida_preproc
        jobs{end+1}='gannetload';
      end

      if include_gannet
        jobs{end+1}='Gannet';
      end

      if exist('subset_options','var') && length(subset_options)>1
        jobs{end+1}='loop_subset';
      else
        subset_options=[]
      end

      if (include_jmrui || include_fida_preproc || include_osprey_fida_preproc) && ~is_ms2sim
        jobs{end+1}='jMRUI';
      end

      if exist('lb_options','var') && length(lb_options)>1
        jobs{end+1}='loop_lb';

        if include_jmrui && ~any(strcmp('jMRUI-inner', jobs)) && ~is_ms2sim % if it's not there from a deeper loop
          jobs{end+1}='jMRUI-inner';
        end
      else
        lb_options=[];
      end

      if exist('conc_options', 'var') && length(conc_options)>1
        jobs{end+1}='loop_conc';
        if exist('simmode_options','var') && length(simmode_options)==0
          jobs{end+1}='jMRUI'; % used to reload cached data
        end
% boolean or logical?
        assert(logical(is_ms2sim),'Can only iterate over concentration/simmode in ms2sim mode');
        assert(logical(continuation), 'Sim mode relies on externally-generated data, must run as a "continuation" of that analysis');
      else
        conc_options=[]
      end

      if exist('simmode_options', 'var') && length(simmode_options)>0
        jobs{end+1}='loop_simmode';
        jobs{end+1}='jMRUI'; % used to reload cached data

        assert(logical(is_ms2sim),'Can only iterate over concentration/simmode in ms2sim mode');
      else
        simmode_options=[]
      end
      if include_standard_basis
        if include_fsl_mrs_noMM_MH
          jobs{end+1}='FSLMRS-fix-MH-noMM';
        end
        if include_fsl_mrs
          if include_fsl_mrs_Newton
            jobs{end+1}='FSLMRS';
          end
          if include_fsl_mrs_noMM
            jobs{end+1}='FSLMRS-noMM';
          end
          if include_fsl_mrs_Grouped
            jobs{end+1}='FSLMRS-Grouped';
            jobs{end+1}='FSLMRS-GlxGroup';
          end
          if include_fsl_mrs_MH
            if include_fsl_mrs_Newton_for_off
              if include_fsl_mrs_NAANAAGgrouped
                jobs{end+1}='FSLMRS-MH-offNewton-NAANAAGgroup';
              else
                jobs{end+1}='FSLMRS-MH-offNewton';
              end
            else
              jobs{end+1}='FSLMRS-MH';
            end
          end
        end
        if include_lcmodel_flat
          jobs{end+1}='LCModel-stdbasis';
        end
        if include_lcmodel
          jobs{end+1}='LCModel-stdbasis-bl06';
        end
        if include_tarquin
          jobs{end+1}='Tarquin-stdbasis';
          if include_tarquin_noMM
            jobs{end+1}='Tarquin-noMM';
          end
        end
      end


      if include_gannet_preproc && include_lcmodel
        jobs{end+1}='Gannet-LCModel';
      end

      if include_gannet_preproc && include_tarquin
        jobs{end+1}='Gannet-Tarquin';
      end

      if include_fida_preproc
        if include_gannet
          jobs{end+1}='FIDA-Gannet';
        end
        if include_lcmodel % {{{
          if ~is_ms2
            jobs{end+1}='LCModel';
          end
          if include_lcmodel_fancy
            jobs{end+1}='LCModel-fancy';
          end
          if include_lcmodel_universal
            jobs{end+1}='LCModel-universal';
          end
          if include_lcmodel_noMM
            jobs{end+1}='LCModel-noMM';
            if include_bl_exploration
              jobs{end+1}='LCModel-noMM-bl06';
            end
          end
          if include_lcmodel_MM_1to1GABA
            jobs{end+1}='LCModel-MM_1to1GABA';
          end
          if include_lcmodel_MM_3to2MM09
            jobs{end+1}='LCModel-MM_3to2MM09';
          end
          if include_lcmodel_MM_GABA_MM09_ART
            jobs{end+1}='LCModel-MM_GABA_MM09_ART';
          end
        end % include_lcmodel }}}
      end

      if include_tarquin
        jobs{end+1}='Tarquin';
      end

      if include_osprey
        jobs{end+1}='osprey_init';

        osprey_suf='';
        osprey_ver=arc_osprey_release();
        osprey_suf=sprintf('-%s',osprey_ver);

        if include_osprey_native_preproc % {{{
          jobs{end+1}=['Osprey-native-sep' osprey_suf];
          if include_osprey_separate_MM_1to1GABA
            jobs{end+1}=['Osprey-native-1to1GABAsoft-bl06' osprey_suf];
          end
          if include_osprey_concat
            jobs{end+1}=[ 'Osprey-native-concat' osprey_suf];
          end
        end % }}}

        if just_get_osprey_scale
          jobs{end+1}=[ 'Osprey-scale' osprey_suf ];
        elseif include_osprey_fida_preproc
          if include_osprey_fida_preproc_concat
            jobs{end+1}=[ 'FIDA-Osprey-concat-X1' osprey_suf];
          end

          if include_osprey_separate

            if include_noMM || include_bl_exploration
              jobs{end+1}=[ 'Osprey-sep' osprey_suf ];
            end

            if extended_mm_exploration
              jobs{end+1}=[ 'Osprey-sep-MM3' osprey_suf ]; % freeGauss14
            end

            jobs{end+1}=[ 'Osprey-sep-MM3-freeGauss' osprey_suf ]; % freeGauss

            if include_bl_exploration
              jobs{end+1}=[ 'Osprey-sep-bl06' osprey_suf ];
            end

            if include_osprey_separate_MM_1to1GABA
              jobs{end+1}=['Osprey-1to1GABAsoft-bl06' osprey_suf ];
              if include_bl_exploration
                jobs{end+1}=['Osprey-1to1GABAsoft-bl04' osprey_suf ];
              end
            end

            if include_osprey_separate_MM_3to2MM09
              jobs{end+1}=[ 'Osprey-3to2MMsoft-bl06' osprey_suf ];
              if include_bl_exploration
                jobs{end+1}=['Osprey-3to2MMsoft-bl04' osprey_suf ];
              end
            end

          end

        end

        jobs{end+1}='osprey_done';

      else
        osprey_suf='xx';
      end

      % add periodic saves
      if length(subset_options)>1 || length(lb_options)>1 || length(conc_options)>1 || length(simmode_options)>1
        jobs{end+1}='save';
        jobs{end+1}='loop';
      end

      % show batch procedure, for visual inspection
      jobs

      pause(3)

      clear('g');
      clear('jm');
      clear('preprocessed');
      clear('preprocessed0');

      jix_subset=[];
      ptr_subset=0; % index within subset list

      jix_lb=[];
      ptr_lb=0;  % index within lb list

      jix_conc=[];
      ptr_conc=0; % index within conc list

      jix_simmode=[];
      ptr_simmode=0; % index within the simmode list


      lb_SNRmatch=-1; % -1: disable, 0: off, 1: on
      do_SNRmatch_this_batch=0;
      SNR_target=-1;

      ji=0;

      default_specs={'diff','off'};
      current_specs=default_specs;
      while (ji<length(jobs))
        ji=ji+1;
        jj=jobs{ji};

        % setup loop indices
        switch jj
          case 'loop_subset'
            jix_subset=ji;
            ptr_subset=ptr_subset+1;
            ptr_lb=0;
            if ((do_snr_match_for_subsets>=0) && (subset_options(ptr_subset)>do_snr_match_for_subsets))
              % NOT REPORTED in current study
              do_SNRmatch_this_batch=1;
            else
              do_SNRmatch_this_batch=0;
            end
            lb_SNRmatch=-1;
          case 'loop_lb'
            show_progress(lb_options, ptr_lb, 'Linewidth 513: ');
            % linebroadening cases: do twice, with/without SNR match (SNR match NOT REPORTED in current study)
            jix_lb=ji;
            ptr_conc=0;
            ptr_simmode=0;
            if do_SNRmatch_this_batch && (lb_SNRmatch>=0 || lb_options(ptr_lb+1)>0)
              if lb_SNRmatch==0
                lb_SNRmatch=1; % NOT REPORTED in current study
              else % -1 or 1: move to next
                ptr_lb=ptr_lb+1;
                lb_SNRmatch=0;
              end
            else
              ptr_lb=ptr_lb+1;
              lb_SNRmatch=-1;
            end
          case 'loop_conc'
            jix_conc=ji;
            ptr_conc=ptr_conc+1;
            ptr_simmode=0;
          case 'loop_simmode'
            jix_simmode=ji;
            ptr_simmode=ptr_simmode+1;
          case 'loop'
            disp(sprintf('----- Done: lb %d/%d, subset %d/%d SNRmatch %d -----', ptr_lb,length(lb_options),ptr_subset,length(subset_options), lb_SNRmatch));
            show_progress(subset_options, ptr_subset,   'Subset 530    : ');
            show_progress(lb_options, ptr_lb,           'Linewidth 529 : ');
            show_progress(conc_options, ptr_conc,       'Conc 531      : ');
            show_progress(simmode_options, ptr_simmode, 'Sim Mode      : ');
            % Resolve loops, innermost first...
            if ptr_simmode<length(simmode_options) % simmost loop
              ji=jix_simmode-1;
              continue;
            elseif ptr_conc<length(conc_options) % conc loop
              ji=jix_conc-1;
              continue;
            elseif ptr_lb<length(lb_options) % inner lb loop
              ji=jix_lb-1;
              continue;
            elseif ptr_subset<length(subset_options) % outer subset loop
              disp('=============== Completed LB cycle ===============');
              lb_options
              ji=jix_subset-1;
              continue;
            else
              disp('***** Fall through all loops *****');
            end
        end % switch

        lab0=jj;

        if strcmp(mode,'MS2sim')
          if strcmp(jj,'jMRUI')
            lab0='sim';
          end
          lab=lab0;
          % lab=sprintf('sim_nomGABA%.2f_lw%.2f_simmode%d', howmuch,lb_target,mode);

          if ptr_conc>0
            lab=[lab sprintf('_nomGABA%.2f',conc_options(ptr_conc))];
          end

          if ptr_lb>0
            lab=[lab sprintf('_lw%.2f',lb_options(ptr_lb))];
          end

          if ptr_simmode>0
            lab=[lab sprintf('_simmode%d',simmode_options(ptr_simmode))];
          end

        else
          lab=lab0;

          if ptr_lb>0
            lab=[lab sprintf('_lb%.1f',lb_options(ptr_lb))];
          end

          if ptr_subset>0
            lab=[lab sprintf('_sub%.2f',subset_options(ptr_subset))];
          end

          if lb_SNRmatch>0
            lab=[lab '_SNRmatch'];
          end
        end

        disp(lab);

        if (dry)
          continue;
        end

        cd(site_folder);
        close all;

        % for the simulation study, only the diff spectra have meaningful background signal variations.
        % hence: only include off spectrum for the basic, pure metabolite mode.
        if is_ms2sim
          if ptr_simmode>0 && simmode_options(ptr_simmode)==1
            current_specs=default_specs;
          else
            current_specs={'diff'}
          end
        end

        try
          switch jj
            case 'loop_subset'
              continue;
            case 'loop_lb'
              show_progress(lb_options, ptr_lb, 'Linewidth 586: ');
              if is_ms2sim
                continue; % externally simulated; nothing to do here.
              end
              preprocessed.reset();
              if ptr_lb>0 && lb_options(ptr_lb)>0
                SNR_target=preprocessed.getSNR('diff');
                warning(sprintf('Applying line-broadening: %.1f',lb_options(ptr_lb)));
                preprocessed.linebroadening(lb_parts,lb_options(ptr_lb),lb_lor_gau);

                SNR_post=preprocessed.getSNR('diff');

                if (lb_SNRmatch>0)
                  % SNRmatch with additive noise is NOT REPORTED in the current work
                  preprocessed.addNoiseForSNRDelta([],SNR_post.SNR-SNR_target.SNR);
                  SNR_afteradd=preprocessed.getSNR('diff');
                end

              end
            case 'gannetload'
              g=MRS_interconnect_Gannet(this_main) % use GannetLoad, which is a little more capable than FID-A's IO functions
              ret=g; % return MRS_interconnect_Gannet
              warning('on'); % Gannet sometimes hides useful warnings. Undo this.
            case 'Gannet'
              % inherits default 'Gannet' label from the loader above
              Gannet_results=g.wrap();
              all_results.append(Gannet_results);
            case 'FSLMRS'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'spec',current_specs));
              clear('fm');
            case 'FSLMRS-noMM'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('noMM',1,'basis_vendor',std_basis_vendor,'spec',current_specs));
              clear('fm');
            case 'FSLMRS-Grouped'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              %all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH NAA+NAAG GABA+MM3co MM09ex','spec','diff'));
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH+GABA+MM3co NAA+NAAG MM09ex','spec','diff'));
              clear('fm');
            case 'FSLMRS-GlxGroup'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH','spec','diff'));
              clear('fm');
            case 'FSLMRS-MH'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'algo','MH','spec',current_specs));
              clear('fm');
            case 'FSLMRS-MH-offNewton'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'algo','MH','spec','diff'));
              if any(strcmp(current_specs,'off'))
                  all_results.append(fm.wrap('basis_vendor',std_basis_vendor,            'spec','off'));
              end
              clear('fm');
            case 'FSLMRS-MH-offNewton-NAANAAGgroup' 
              % see supplementarty analysis; intended to address fit
              % instability in very low linewidth cases, but not necessarily an
              % improvement
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'algo','MH','spec','diff','metab_groups','NAA+NAAG'));
              if any(strcmp(current_specs,'off'))
                  all_results.append(fm.wrap('basis_vendor',std_basis_vendor,            'spec','off'));
              end
              clear('fm');
            case 'jMRUI'

              if is_ms2sim
                assert(logical(continuation))
                jm=MRS_interconnect_jMRUI(g,'label',lab)
                if jm.reload_preproc()<=0
                  warning('No prior preproc could be found.');
                  jm=[];
                end
              else
                jm0=MRS_interconnect_jMRUI(g,'label',lab0)

                if (enable_caching)
                  warning('jMRUI cache read')
                  jm0.cache_read();
                else
                  warning('jMRUI  : no cache ');
                end

                if enable_manipulations && ptr_subset>0 && subset_options(ptr_subset)<1
                  % don't cache all the subsets; this otherwise gets prohibitively huge
                  if continuation
                    ilab=replace(lab,'jMRUI_lb','jMRUI-inner_lb');
                    jm=MRS_interconnect_jMRUI(jm0,'label',ilab); % copy to new structure for new processing
                    jm.reload_preproc();
                    jm.compare(jm0);
                  else
                    jm=MRS_interconnect_jMRUI(jm0,'label',lab); % copy to new structure for new processing
                    jm.preprocess('subset',subset_options(ptr_subset),'subset_blocksize',subset_blocksize);
                  end
                elseif ~jm0.is_preprocessed()
                  jm0.preprocess();
                  if (enable_caching)
                    assert(~is_ms2sim); % don't inadvertently replace any of our simulated data
                    jm0.cache_write(); 
                  end
                  jm=jm0;
                  clear('jm0');
                end
              end

              jm.compare(g); % this exports a figure called comparison_metab, which can be used for visual inspection
              if include_jmrui
                if continuation
                  error('Refusing to update jmrui-format data in "continuation" mode. Doing so would break continuity.')
                end
                jm.write();
              end

              if include_simple_integral
                % basic integration for validation
                simple_integral=jm.getBasicIntegral();
                all_results.append(simple_integral);
              end
              % record basic SNR/linewidth metrics from FIDA preproc
              preproc_info=jm.getQualityInfo(); % 'spec','diff'
              preproc_info.show();

              all_results.append(preproc_info);

              ret=jm; % return MRS_interconnect_jMRUI
              preprocessed0=jm; % this may be a reduced-SNR subset, but otherwise unmanipulated

              if enable_manipulations
                preprocessed=MRS_manipulator(preprocessed0);
                if do_SNRmatch_this_batch
                  % NOT REPORTED in the current work
                  preprocessed.model_lw_noise();
                end
              else
                preprocessed=preprocessed0;
              end
              preprocessed=preprocessed0;

              
              if ~include_gannet_preproc && ~enable_manipulations
                clear('g'); % won't be needing this one anymore.
              end
              clear('jm','preprocessed0');

            case 'jMRUI-inner'
              jm=MRS_interconnect_jMRUI(preprocessed,'label',lab);
              jm.write();
              preproc_info=jm.getQualityInfo(); % 'spec','diff'
              preproc_info.show();
              all_results.append(preproc_info);
              clear('jm');

            case 'Gannet-LCModel'
              l=MRS_interconnect_LCModel(g,'label',lab);
              LCM_results=l.wrap();
              LCM_results.show();
              all_results.append(LCM_results);
              all_results.append(l.internal_quantify_water());
              clear('l');
            case 'Gannet-Tarquin'
              t=MRS_interconnect_tarquin(g,'label',lab);
              TQ_results=t.wrap();
              TQ_results.show();
              all_results.append(TQ_results);
              all_results.append(t.internal_quantify_water());
              clear('t');
            case 'FIDA-Gannet'
              g2=MRS_interconnect_Gannet(preprocessed,'label',lab);
              all_results.append(g2.wrap());
              clear('g2');
            case 'LCModel'
              l2=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l2.wrap('spec',current_specs));
              all_results.append(l2.internal_quantify_water());
              clear('l2');
            case 'LCModel-fancy'
              l3=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l3.wrap('fancy_baseline',1));
              clear('l3');
            case 'LCModel-universal'
              l4=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l4.wrap('universal_basis',1));
              clear('l4');
            case 'LCModel-stdbasis'
              l4b=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l4b.wrap('basis_vendor',std_basis_vendor,'spec',current_specs));
              clear('l4b');
            case 'LCModel-noMM'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','noMM',1,'basis_vendor',std_basis_vendor));
              clear('l7');
            case 'LCModel-stdbasis-bl06'
              l4b=MRS_interconnect_LCModel(preprocessed,'label',lab);
              if is_ms2sim && ptr_simmode>0 && simmode_options(ptr_simmode)==0
                % fixed no shift in simulated GABA-only case
                % DOREFS=F,F
                % PPMSHF=0.0
                % --- alternatively --
                % nrefpk(2)=1
                % ppmref(1,2)=3.02 % GABA
                % shifmn(2), shifmx(2)...
                all_results.append(l4b.wrap('basis_vendor',std_basis_vendor,'dkntmn',.6,'noshift',1,'spec',current_specs));
              else
                % in all other cases, use default shift and refs
                all_results.append(l4b.wrap('basis_vendor',std_basis_vendor,'dkntmn',.6,'spec',current_specs));
              end
              clear('l4b');
            case 'LCModel-noMM-bl06'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','noMM',1,'dkntmn',.6,'basis_vendor',std_basis_vendor));
              clear('l7');
            case 'LCModel-MM_1to1GABA'
              l5=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l5.wrap('spec','diff','constraint_model','1to1GABAsoft','basis_vendor',std_basis_vendor));
              clear('l5');
            case 'LCModel-MM_3to2MM09'
              l6=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l6.wrap('spec','diff','constraint_model','3to2MMsoft','basis_vendor',std_basis_vendor));
              clear('l6');
            case 'LCModel-MM_GABA_MM09_ART'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','constraint_model','GABA_MM09_ART','basis_vendor',std_basis_vendor));
              clear('l7');
            case 'Tarquin'
              t2=MRS_interconnect_tarquin(preprocessed,'label',lab);
              all_results.append(t2.wrap('spec',current_specs));
              all_results.append(t2.internal_quantify_water());
              clear('t2');
            case 'Tarquin-stdbasis'
              t3=MRS_interconnect_tarquin(preprocessed,'label',lab);
              % can be quite slow with more complicated basis sets and low-quality data... optionally disable edit-off
              if include_tarquin_stdbasis_off
                incl=current_specs; % {'diff','off'};
              else
                incl={'diff'};
              end
              all_results.append(t3.wrap('spec',incl,'basis_vendor',std_basis_vendor));
              clear('t3');
            case 'Tarquin-noMM'
              t3=MRS_interconnect_tarquin(preprocessed,'label',lab);
              all_results.append(t3.wrap('spec','diff','noMM',1,'basis_vendor',std_basis_vendor));
              clear('t3');

            % Osprey native (just sep and 1to1 bl06 now) {{{
            case [ 'Osprey-native-sep' osprey_suf ]
              os=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(os.wrap('fit_style','Separate'));
              clear('os');   % osprey struct gets huge!
            case ['Osprey-native-1to1GABAsoft-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.6));
              clear('os');
            case ['Osprey-native-concat' osprey_suf ]
              o=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(o.wrap());
              clear('o');
            % }}}

            % Osprey with FID-A preproc {{{

            case [ 'FIDA-Osprey-concat-X1' osprey_suf ]
              fo=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fo.wrap());
              clear('fo'); 

            case ['Osprey-sep' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate'));
              clear('fos');

            case ['Osprey-scale' osprey_suf ]
              % just get initial scaling information from Osprey, without running a fit (these details were not exported initially)
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','just_get_scale',1));
              clear('fos'); % gets huge otherwise

            case ['Osprey-sep-MM3' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','freeGauss14'));
              clear('fos'); 

            case ['Osprey-sep-MM3-freeGauss' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','freeGauss'));
              clear('fos'); 

            case ['Osprey-sep-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','bLineKnotSpace',0.6));
              clear('os');   

            case ['Osprey-1to1GABAsoft-bl04' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.4));
              clear('os');   

            case ['Osprey-1to1GABAsoft-bl06' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.6));
              clear('fos');   

            case [ 'Osprey-3to2MMsoft-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','3to2MMsoft','bLineKnotSpace',0.6));
              clear('os');   

            case [ 'Osprey-3to2MMsoft-bl04' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','3to2MMsoft','bLineKnotSpace',0.4));
              clear('os');   

            % }}}

            case 'osprey_init' % special case: switch to Osprey's local FID-A variant
              force_FIDA('osprey'); % switch to Osprey's local FID-A version

            case 'osprey_done' % special case: switch back to compatible FID-A version
              force_FIDA('compat');  % use standard upstream (ish) FID-A

            case 'save' % save output csv
              % results table can get cumbersome with multiple nested loops; break it into smaller chunks
              if (min(size(all_results))>0) % ...but also check that we actually have anything to write {{{
                disp(['Saving results to: ' output_filename]);
                all_results.save(output_filename);

                % incremental output
                all_results=datatable_ARC();
                output_filename_inc=output_filename_inc+1;
                output_filename=sprintf('%s-%03d.csv', output_filename_base, output_filename_inc);
              else % }}}
                disp('No results to save yet.')
              end

            otherwise
              if ~strncmp('loop_',jj,5)
                warning(['Unknown job: ' jj]);
              end
          end
          warning('on'); % Some tools (eg: gannet) occasionally hide useful warnings. make sure these do not remain hidden.
        catch Ex
          warning('on');
          ex_formatted=formatting.format_exception(Ex);
          %failures(files{i})=ex_formatted;
          warning(ex_formatted);
          warning([jj ' failed for ' this_main]);
          switch jj
            case 'gannetload'
              warning('Initial load failed; cannot proceed');
              rethrow(Ex);
            case 'jMRUI'
              warning('Initial load failed; cannot proceed');
              rethrow(Ex);
            otherwise
              if stop_on_error
                rethrow(Ex);
              end
          end
        end
      end

    % }}}
    catch Ex
      ex_formatted=formatting.format_exception(Ex);
      %failures(files{i})=ex_formatted;
      warning(ex_formatted);
      warning(['Processing failed for ' this_main]);
      if stop_on_error
        rethrow(Ex)
      end
    end

    cd(owd);

    try % write out results {{{
      disp('Results for all scans:');
      all_results.show();

      disp(['Saving results to: ' output_filename]);
      if dry
        disp('(dry run; not actually writing)');
      else
        all_results.save(output_filename);
      end
      % }}}
    catch Ex % no, writing did not work. {{{
      ex_formatted=formatting.format_exception(Ex);
      %failures(files{i})=ex_formatted;
      if stop_on_error
        rethrow(Ex);
      else
        warning(ex_formatted);
      end
    end % }}}
  end % for each subject }}}
end % process_list

function show_progress(opts, ix, prefix) % {{{
  % show_progress : display available options for loop variable, highlighting
  % the current index

  if nargin>2
    st=prefix;
  else
    st='';
  end
  if length(opts)==0 && length(st)>0
    disp([ st 'not changing.']);
    return
  end
  if ix>0
    for i=1:ix-1
      st=[st sprintf(' %3.1f ', opts(i))];
    end
    st=[st sprintf('[%3.1f]', opts(ix))];
  else
    ix=0;
  end
  for i=ix+1:length(opts)
    st=[st sprintf(' %3.1f ', opts(i))];
  end
  disp(st);
end % }}}
