# Linewidth-related bias in modelled concentration estimates from GABA-edited <sup>1</sup>H-MRS

## Background

This repository contains scripts for processing, analysis and reporting of outcomes from various different modelling algorithms for GABA-edited <sup>1</sup>H-MRS, subject to differing linewidth, lineshape and SNR conditions. It is intended as a supplement to our forthcoming manuscript on the subject.

The pipeline presented here is based on one used in a previous work ([published in NMR in Biomedicine](https://doi.org/10.1002/nbm.4702), [preprint available on bioRxiv](https://doi.org/10.1101/2021.11.15.468534)), which is available [here](https://git.app.uib.no/bergen-fmri/analyzing-big-gaba).

Please note that many of these scripts are tailored to this specific purpose and to our local processing environment: prospective users will need to update file locations in accordance with their local folder structure.

## Key Details

The present work explores the impact of SNR and lineshape on quantification estimates. The underlying linebroadening function is implemented in [`op_filter_lg`](processing/op_filter_lg.m), which implements an adjustable mix of Lorenzian and Gaussian linebroadening. This is invoked through a wrapper function in [`MRS_manipulator`](processing/MRS_manipulator.m), which in turn is called through the main loop in [`02_process_list.m`](processing/process_list.m).

## Stages of Analysis

### `01_gather_inputs`

Searches a directory for datasets in the expected structure. Metabolite and unsuppressed water reference data are paired (where applicable), and grouped according to site. The resulting lists are exported into a matlab-format cell array. Typically, output should be saved to a file -- for example, from `bash`-like command line:

```shell
./01_gather_inputs > generated_gaba68.m
```

### `02_process_list.m` (`process_list.m`)

Iterates over a list of datasets, and applies a pre-defined set of processing and quantification steps. Typical usage (in matlab):

```matlab
generated_gaba68;   % load list of subjects
process_list(subs);
```

This periodically writes outcomes in CSV format, to a timestamped file in the working directory, eg: `/data/big_gaba/S8_MP/output.20210507.0113.csv`).

While some algorithms are quite fast, others may take several minutes to complete a single fit. For the full analysis described in our manuscript (6 SNR steps, 22 LB steps, for Gaussian and Lorentzian LB, edit-OFF and diff spectra) there are 528 fits per subject, per algorithm (703,296 fits total). On a capable machine, a single subject may well require 5-10 hours of fitting, the entire dataset a couple of months.

### `03_gather_outputs.py`

Search the working directory for the most recent output CSV for each set, and combine these into a single timestamped file, eg: `./merged.20210512.1142.csv`

If specified with the `latest` parameter (eg: `./03_gather_outputs.py latest`), the latest merged file is pre-loaded, allowing for incremental building of a complete result set.

### `04_gather_fits.py`

Search the working directory for output spectral data in each algorithm's respective format, import these, adjust to a common scale. Implements initial rejection of bad spectra (*R1*) and generation of all spectral figures.

## Support Functions

- `setup_submodules.m` checks that all necessary components are configured in the Matlab path (called near startup of `process_list.m`)
- `force_FIDA.m` switches between a standard FID-A release, and Osprey's altered version (major differences include the conjugate sense of FFTs, and assumptions relating to center frequency). Called when needed by `process_list.m`
- `standard_basis.m` takes the simulated basis sets shipped with Osprey, and assembles them into per-vendor basis sets in LCModel format, at all the necessary resolutions, with and without a simulated MM3 component (the present work only reports from basis sets including the MM3 component).

## Requirements and Dependencies

All spectral analysis software packages to be considered must be installed, in accordance with the respective supplier's directions:

- [FSL-MRS](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/)
- [Gannet](http://www.gabamrs.com/downloads)
- [LCModel](http://www.s-provencher.com/lcmodel.shtml)
- [Osprey](https://schorschinho.github.io/osprey/)
- [Tarquin](https://github.com/martin3141/tarquin/)

The preprocessing pipeline is largely implemented around the methods of FID-A:

- [FID-A](https://github.com/CIC-methods/FID-A)

Scheduling and exchange of spectral data and results between the various algorithms is facilitated using the locally-implemented MRS_interconnect library:

- [MRS_interconnect](https://git.app.uib.no/bergen-fmri/MRS_interconnect)

Some source data is also necessary. For our analysis, we used TE=68ms (GABA+) data from the publicly-available [Big GABA dataset](https://www.nitrc.org/projects/biggaba)
